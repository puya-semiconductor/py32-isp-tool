
// PY32IspToolDlg.h : header file
//

#pragma once
#include "HcComboBox.h"
#include "PuyaDevice.h"
#include "PuyaISP.h"
#include "XProgressCtrl.h"
#include "py_program_protocol.h"

typedef enum _ThreadFuncID
{
	ID_TargetConnect = 0,
	ID_TargetDisconnect,
	ID_TargetDownload,
	ID_TargetEraseFullChip,
	ID_TargetEraseSectors,
	ID_TargetErasePages,
	ID_TargetProgram,
	ID_TargetVerify,
	ID_TargetBlankCheck,
	ID_TargetRunApp,
	ID_TargetProgramOptionBytes,
	ID_TargetReadData,

	ID_DeviceUpdateFile,

	ThreadFuncID_MAX
}ThreadFuncID;

const CString FUNC_TEXT[ThreadFuncID_MAX] = {
	_T("TargetConnect"),
	_T("TargetDisconnect"),
	_T("TargetDownload"),
	_T("TargetEraseFullChip"),
	_T("TargetEraseSectors"),
	_T("TargetErasePages"),
	_T("TargetProgram"),
	_T("TargetVerify"),
	_T("TargetBlankCheck"),
	_T("TargetRunApp"),
	_T("TargetProgramOptionBytes"),
	_T("TargetReadData"),

	_T("DeviceUpdateFile"),
};

CONST CString DTR_RTS_STR[] = {
	_T("DTR and RTS are not used"),
	_T("DTR low level(< -3V) reset without RTS"),
	_T("DTR low level(< -3V) reset, RTS low level Bootloader"),
	_T("DTR low level(< -3V) reset, RTS high level Bootloader"),
	_T("DTR high level(> +3V) reset without RTS"),
	_T("DTR high level(> +3V) reset, RTS low level Bootloader"),
	_T("DTR high level(> +3V) reset, RTS high level Bootloader"),
	_T("RTS low level(< -3V) reset without DTR"),
	_T("RTS low level(< -3V) reset, DTR low level Bootloader"),
	_T("RTS low level(< -3V) reset, DTR high level Bootloader"),
	_T("RTS high level(> +3V) reset without DTR"),
	_T("RTS high level(> +3V) reset, DTR low level Bootloader"),
	_T("RTS high level(> +3V) reset, DTR high level Bootloader")
};

CONST DWORD BAUD_RATE[] = {
	9600, 14400, 19200, 38400, 56000, 57600, 115200, 128000, 256000, 460800, 1000000
};

// CPY32IspToolDlg dialog
class CPY32IspToolDlg : public CDialogEx
{
// Construction
public:
	CPY32IspToolDlg(CWnd* pParent = nullptr);	// standard constructor
	~CPY32IspToolDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PY32ISPTOOL_DIALOG };
#endif
	int m_nRadioErase;
	CFont m_Font;
	CEdit m_EditLog;
	CXProgressCtrl m_XProgressCtrl;
	CHcComboBox m_ComboBoxIspDevice;
	CHcComboBox m_ComboBoxBaudrate;
	CHcComboBox m_HcComboBoxDtrRts;

	int m_nProgressPos;

	CPuyaDevice* m_pDevice;
	FILE_TypeDef FILE_InitStruct;
	CHIP_TypeDef CHIP_InitStruct;
	BYTE* m_pucDeviceInfo;
	BYTE* m_pucFlashBlob;
	BYTE* m_pucProgramMemory;
	BYTE* m_pucTargetMemory;
	WORD m_wPID;
	DWORD m_dwComPort;
	ThreadFuncID m_eThreadFuncID;

	BOOL m_bReadMemoryOneTime;

	BOOL m_bExitApp;

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg BOOL OnDeviceChange(UINT nEventType, DWORD_PTR dwData);
	DECLARE_MESSAGE_MAP()
public:
	static UINT ThreadProc(LPVOID lParam);
	void ThreadFunc(void);
	void UpdateStatus(const TCHAR* szFormat, ...);
	BOOL OpenFile(LPCTSTR lpszPathName);
	void EnableCtrl(BOOL bEnable);

	void SetStructData(uint32_t mcu_pid);

	uint32_t TargetDownload();
	uint32_t TargetConnect();
	uint32_t TargetReadMcuInfo();
	uint32_t TargetDisconnect();
	uint32_t TargetEraseFullChip();
	uint32_t TargetEraseSectors();
	uint32_t TargetErasePages();
	uint32_t TargetBlankCheck();
	uint32_t TargetProgram();
	uint32_t TargetVerify();
	uint32_t TargetReadData();
	uint32_t TargetRunApp();
	uint32_t TargetProgramOptionBytes();

	uint8_t ReadMemory(uint32_t addr, uint8_t* data, uint32_t size);
	BOOL SetDtrAndRts(bool bSetBoot0);


	afx_msg void OnBnClickedCheckProgram();
	afx_msg void OnBnClickedCheckVerify();	
	afx_msg void OnBnClickedCheckProgramOptionBytes();
	afx_msg void OnBnClickedCheckRunApp();

	afx_msg void OnBnClickedButtonDnloadOpen();
	afx_msg void OnBnClickedButtonRunApp();
	afx_msg void OnBnClickedButtonUpload();
	afx_msg void OnBnClickedButtonDnload();
	afx_msg void OnCbnSelchangeComboIspDevice();
	afx_msg void OnBnClickedButtonConnect();
	afx_msg void OnCbnSelchangeComboBaudrate();
	afx_msg void OnCbnSelchangeComboDtrRts();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
