﻿#pragma once

#define MAX_PROPERTY_ITEM 30

// COptionBytesDlg 对话框

class COptionBytesDlg : public CDialogEx
{
	DECLARE_DYNAMIC(COptionBytesDlg)

public:
	COptionBytesDlg(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~COptionBytesDlg();

	void AdjustLayout();
	void InitPropList();
	void SetPropListFont();

	BYTE* m_pucOptionBytes;
	WORD m_wPID;

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_OPTION_BYTES };
#endif

protected:
	CFont m_fntPropList;
	CMFCPropertyGridCtrl m_wndPropList;
	CMFCPropertyGridProperty* m_pPropertyItem[MAX_PROPERTY_ITEM];

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBnClickedOk();
};
