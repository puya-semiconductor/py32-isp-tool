#include "pch.h"
#include "ParseHexFile.h"

BOOL CParseHexFile::HexLineText2HexLineStruct(void)
{
	CString csText;
	BYTE ucSum = 0x00;
	BYTE ucData[1 + 2 + 1 + 256 + 1];
	m_csHexLineText.Replace(_T(":"), _T(""));
	for (int i = 0; i < m_csHexLineText.GetLength() / 2; i++)
	{
		csText = m_csHexLineText.Mid(i * 2, 2);
		ucData[i] = (BYTE)_tcstoul(csText, NULL, 16);
		ucSum += ucData[i];
	}
	if (0x00 != ucSum)
	{
		return FALSE;
	}
	m_tHexLineStruct.ucDataLength = ucData[0x00];
	m_tHexLineStruct.wDataAddr = (ucData[0x01] << 8) + ucData[0x02];
	m_tHexLineStruct.ucDataType = ucData[0x03];
	for (int i = 0; i < ucData[0x00]; i++)
	{
		m_tHexLineStruct.ucData[i] = ucData[0x04 + i];
	}
	return TRUE;
}

void CParseHexFile::HexLineStruct2HexLineText(void)
{
	BYTE* pucData;
	BYTE ucSum = 0x00;

	pucData = new BYTE[4 + m_tHexLineStruct.ucDataLength];

	pucData[0x00] = m_tHexLineStruct.ucDataLength;
	pucData[0x01] = (m_tHexLineStruct.wDataAddr >> 8) & 0xFF;
	pucData[0x02] = (m_tHexLineStruct.wDataAddr >> 0) & 0xFF;
	pucData[0x03] = m_tHexLineStruct.ucDataType;
	for (int i = 0; i < 4; i++)
	{
		ucSum += pucData[i];
	}
	for (int i = 0; i < m_tHexLineStruct.ucDataLength; i++)
	{
		pucData[0x04+i] = m_tHexLineStruct.ucData[i];
		ucSum += m_tHexLineStruct.ucData[i];
	}
	pucData[0x04+ m_tHexLineStruct.ucDataLength] = 0x100 - ucSum;

	m_csHexLineText = _T(":");
	for (BYTE i = 0; i < 0x04 + m_tHexLineStruct.ucDataLength + 0x01; i++)
	{
		m_csHexLineText.AppendFormat(_T("%02X"), pucData[i]);
	}
	m_csHexLineText += _T("\n");

	delete []pucData;
}

BOOL CParseHexFile::ReadHexFile(CString csHexFileName, BYTE* pucData, DWORD& dwAddr, DWORD& dwLength)
{
	CStdioFile File;
	DWORD dwExtendedLinearAddress = 0;

	if (!File.Open(csHexFileName, CFile::modeRead)) {
		return FALSE;
	}

	while (File.ReadString(m_csHexLineText))
	{
		if (m_csHexLineText.IsEmpty()) {
			continue;
		}
		m_csHexLineText.Trim();
		if (!HexLineText2HexLineStruct())
		{
			File.Close();
			return FALSE;
		}
		switch (m_tHexLineStruct.ucDataType)
		{
		case 0://Data Record//数据记录
			dwLength += m_tHexLineStruct.ucDataLength;
			memcpy(&pucData[(dwExtendedLinearAddress & 0x00FFFFFF) + m_tHexLineStruct.wDataAddr], m_tHexLineStruct.ucData, m_tHexLineStruct.ucDataLength);
			break;
		case 1://End of File Record//文件结束记录
			break;
		case 2://Extended Segment Address Record//扩展段地址记录
			break;
		case 3://Start Segment Address Record//开始段地址记录
			break;
		case 4://Extended Linear Address Record//扩展线性地址记录			
			dwExtendedLinearAddress = ((m_tHexLineStruct.ucData[0x00] << 8) + m_tHexLineStruct.ucData[0x01]) << 16;
			if (0x00000000 == dwAddr) {
				dwAddr = dwExtendedLinearAddress;
			}
			break;
		case 5://Start Linear Address Record//开始线性地址记录
			break;
		}
	}

	File.Close();

	return TRUE;	
}

BOOL CParseHexFile::SaveHexFile(CString csHexFileName, BYTE* pucData, DWORD dwAddr, DWORD dwLength)
{
	CFile File;

	if (!File.Open(csHexFileName, CFile::modeWrite | CFile::modeCreate)) {
		return FALSE;
	}

	//CArchive ar(&File, CArchive::store);

	//SaveHexFile(ar, pucData, dwAddr, dwLength);

	//ar.Close();

	return TRUE;
}
