#ifndef __DATA_HANDLE_MACRO_H
#define __DATA_HANDLE_MACRO_H

#define __IO

//#define __IO volatile
//typedef uint8_t BYTE;
//typedef uint16_t WORD;
//typedef uint32_t DWORD;

#define M8(adr) (*((__IO BYTE *) (adr)))
#define M16(adr) (*((__IO WORD *) (adr)))
#define M32(adr) (*((__IO DWORD *) (adr)))

#define SET_BIT(REG, BIT)     ((REG) |= (BIT))
#define CLEAR_BIT(REG, BIT)   ((REG) &= ~(BIT))
#define SET_BIT_DATA(REG, BIT, DATA) (DATA ? SET_BIT(REG, BIT) : CLEAR_BIT(REG, BIT))
#define READ_BIT(REG, BIT)    ((REG) & (BIT))
#define CLEAR_REG(REG)        ((REG) = (0x0))
#define WRITE_REG(REG, VAL)   ((REG) = (VAL))
#define READ_REG(REG)         ((REG))
#define MODIFY_REG(REG, CLEARMASK, SETMASK)  WRITE_REG((REG), (((READ_REG(REG)) & (~(CLEARMASK))) | (SETMASK)))
#define CLEAR_WPBIT(REG, CLEARMASK, WPKEY) WRITE_REG((REG), ((READ_REG(REG)) & (~(CLEARMASK))) | WPKEY)
#define POSITION_VAL(VAL)     (__CLZ(__RBIT(VAL))) 
//#define UNUSED(X) (void)X      /* To avoid gcc/g++ warnings */
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))

#endif //__DATA_HANDLE_MACRO_H