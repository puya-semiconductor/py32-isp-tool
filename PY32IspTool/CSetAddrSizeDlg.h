﻿#pragma once
#include "afxdialogex.h"


// CSetAddrSizeDlg 对话框

class CSetAddrSizeDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSetAddrSizeDlg)

public:
	CSetAddrSizeDlg(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~CSetAddrSizeDlg();

	DWORD m_dwAddr;
	DWORD m_dwSize;

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_ADDR_SIZE };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
};
