#pragma once


class CHcComboBox : public CComboBox
{
public:
	CHcComboBox();
	virtual ~CHcComboBox();

protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
};
