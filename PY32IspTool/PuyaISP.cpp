#include "pch.h"
#include "PuyaISP.h"

#define CMD_Get													0x00
#define CMD_GetVersionAndReadProtectionStatus					0x01
#define CMD_GetID												0x02
#define CMD_ReadMemory											0x11
#define CMD_Go													0x21
#define CMD_WriteMemory											0x31
#define CMD_Erase												0x43
#define CMD_ExtendedErase										0x44
#define CMD_WriteProtect										0x63
#define CMD_WriteUnprotect										0x73
#define CMD_ReadoutProtect										0x82
#define CMD_ReadoutUnprotect									0x92

bool CPuyaISP::WaitData(const uint8_t ucData)
{
	uint8_t ucRead;
	if (!ReadDataEx(&ucRead, 1))
	{
		return false;
	}
	if (ucData != ucRead)
	{
		return false;
	}
	return true;
}

uint32_t CPuyaISP::Init(uint32_t nPort, uint32_t nSpeed)
{
	BYTE ucData[MAX_PATH] = { 0x7F };	

	if (NULL == m_pCnComm) {
		m_pCnComm = new CnComm;
		if (!m_pCnComm->Open(nPort, nSpeed, EVENPARITY)) {
			return ERROR_COM_OPEN;
		}
	}

	SetDtrAndRts(true);

	do
	{
		m_pCnComm->Purge(PURGE_RXCLEAR);
		for (int i = 0; i < 10; i++) {
			ucData[0x00] = 0x7F;			
			m_pCnComm->Write(ucData, 1);

			if (ReadDataEx(ucData+1, 1, 10)) {
				break;
			}
		}

		if ((ACK != ucData[1]) && (NACK != ucData[1])) {
			return ERROR_ISP_0x7F;
		}

		if (!Get()) {
			return ERROR_ISP_Get;
		}

		if (!GetID()) {
			return ERROR_ISP_GetID;
		}

		if (ERROR_SUCCESS == ReadMemory(0x20000800, ucData, 0x04))
		{
			return ERROR_SUCCESS;
		}

		if (IDNO == AfxMessageBox(_T("MCU read protection active, do you want to remove read protection?"), MB_YESNO | MB_ICONQUESTION))
		{
			return ERROR_IAP_READ;
		}

		if (!ReadoutUnprotect())
		{
			return ERROR_ISP_ReadoutUnprotect;
		}

		Sleep(500);
	} while (1);

	return ERROR_SUCCESS;
}

uint32_t CPuyaISP::Uninit(void)
{
	SetDtrAndRts(false);

	return ERROR_SUCCESS;
}

uint32_t CPuyaISP::WriteMemory(uint32_t addr, uint8_t* data, uint32_t size)
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = CMD_WriteMemory;
	ucData[0x01] = 0xFF - CMD_WriteMemory;

	m_pCnComm->Write(ucData, 0x02);
	if (!WaitData(ACK)) {
		return ERROR_IAP_WRITE;
	}
	ucData[0x00] = HIBYTE(HIWORD(addr));
	ucData[0x01] = LOBYTE(HIWORD(addr));
	ucData[0x02] = HIBYTE(LOWORD(addr));
	ucData[0x03] = LOBYTE(LOWORD(addr));
	ucData[0x04] = GetXOR(ucData, 0x04);
	m_pCnComm->Write(ucData, 0x05);
	if (!WaitData(ACK)) {
		return ERROR_IAP_WRITE;
	}

	ucData[0x00] = size - 1;
	m_pCnComm->Write(ucData, 0x01);

	memcpy(&ucData[0x01], data, size);
	m_pCnComm->Write(&ucData[0x01], size);

	ucData[size + 1] = GetXOR(ucData, size + 1);
	m_pCnComm->Write(&ucData[size + 1], 0x01);
	if (!WaitData(ACK)) {
		return ERROR_IAP_WRITE;
	}

	return ERROR_SUCCESS;
}

uint8_t  CPuyaISP::ReadMemory(uint32_t addr, uint8_t* data, uint32_t size)
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = CMD_ReadMemory;
	ucData[0x01] = 0xFF - CMD_ReadMemory;

	m_pCnComm->Write(ucData, 0x02);
	if (!WaitData(ACK)) {
		return ERROR_IAP_READ;
	}

	ucData[0x00] = HIBYTE(HIWORD(addr));
	ucData[0x01] = LOBYTE(HIWORD(addr));
	ucData[0x02] = HIBYTE(LOWORD(addr));
	ucData[0x03] = LOBYTE(LOWORD(addr));
	ucData[0x04] = GetXOR(ucData, 0x04);
	m_pCnComm->Write(ucData, 0x05);
	if (!WaitData(ACK)) {
		return ERROR_IAP_READ;
	}

	ucData[0x00] = size - 1;
	ucData[0x01] = 0xFF - ucData[0x00];
	m_pCnComm->Write(ucData, 0x02);
	if (!WaitData(ACK)) {
		return ERROR_IAP_READ;
	}

	if (!ReadDataEx(data, size, 3)) {
		return ERROR_IAP_READ;
	}

	return ERROR_SUCCESS;
}

uint32_t CPuyaISP::ErasePage(uint8_t* data, uint32_t size)
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = 0x10;
	ucData[0x01] = size - 1;

	memcpy(ucData + 2, data, size * 2);

	if (!ExtendedErase(ucData, (0x01 + size) * 2))
	{
		return ERROR_IAP_ERASE_PAGE;
	}
	return ERROR_SUCCESS;
}

uint32_t CPuyaISP::EraseSector(uint8_t* data, uint32_t size)
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = 0x20;
	ucData[0x01] = size - 1;

	memcpy(ucData + 2, data, (size_t)(size * 2));

	if (!ExtendedErase(ucData, (0x01 + size) * 2))
	{
		return ERROR_IAP_ERASE_SECTOR;
	}
	return ERROR_SUCCESS;
}

uint32_t CPuyaISP::EraseChip(void)
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = 0xFF;
	ucData[0x01] = 0xFF;

	if (!ExtendedErase(ucData, (CMD_ExtendedErase == m_ucEraseCmd) ? 0x02 : 0x01))
	{
		return ERROR_IAP_ERASE_ALL;
	}

	return ERROR_SUCCESS;
}

uint8_t CPuyaISP::Go(uint32_t addr)
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = CMD_Go;
	ucData[0x01] = 0xFF - CMD_Go;
	
	m_pCnComm->Write(ucData, 0x02);
	if (!WaitData(ACK)) {
		return ERROR_IAP_RUN_APP;
	}

	ucData[0x00] = HIBYTE(HIWORD(addr));
	ucData[0x01] = LOBYTE(HIWORD(addr));
	ucData[0x02] = HIBYTE(LOWORD(addr));
	ucData[0x03] = LOBYTE(LOWORD(addr));
	ucData[0x04] = GetXOR(ucData, 0x04);
	m_pCnComm->Write(ucData, 0x05);
	if (!WaitData(ACK)) {
		return ERROR_IAP_RUN_APP;
	}

	return ERROR_SUCCESS;
}

//uint32_t CPuyaISP::WriteOptionBytes(uint32_t addr, uint8_t* data, uint32_t size)
//{	
//	uint32_t nErrCode;
//
//	nErrCode = WriteMemory(addr, data, size);
//	if (ERROR_SUCCESS != nErrCode) {
//		return nErrCode;
//	}
//
//	//nErrCode = Init(0, 0);
//	//if (ERROR_SUCCESS != nErrCode) {
//	//	return nErrCode;
//	//}
//
//	return ERROR_SUCCESS;
//}

bool CPuyaISP::Get()
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = CMD_Get;
	ucData[0x01] = 0xFF - CMD_Get;

	m_pCnComm->Write(ucData, 0x02);

	if (!WaitData(ACK)) {
		return false;
	}

	if (!ReadDataEx(ucData, 1)) {
		return false;
	}

	if (!ReadDataEx(ucData + 1, 1)) {
		return false;
	}

	if (!ReadDataEx(ucData + 2, ucData[0])) {
		return false;
	}

	if (!WaitData(ACK)) {
		return false;
	}

	for (int i = 0; i < ucData[0]; i++) {
		if ((CMD_Erase == ucData[i + 2]) ||
			(CMD_ExtendedErase == ucData[i + 2])) {
			m_ucEraseCmd = ucData[i + 2];
			break;
		}
	}

	return true;
}

bool CPuyaISP::GetVersionAndReadProtectionStatus()
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = CMD_GetVersionAndReadProtectionStatus;
	ucData[0x01] = 0xFF - CMD_GetVersionAndReadProtectionStatus;

	m_pCnComm->Write(ucData, 0x02);

	if (!WaitData(ACK)) {
		return false;
	}

	if (!ReadDataEx(ucData, 1)) {
		return false;
	}

	if (!ReadDataEx(ucData + 1, 2)) {
		return false;
	}

	if (!WaitData(ACK)) {
		return false;
	}

	return true;
}

bool CPuyaISP::GetID()
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = CMD_GetID;
	ucData[0x01] = 0xFF - CMD_GetID;

	m_pCnComm->Write(ucData, 0x02);

	if (!WaitData(ACK)) {
		return false;
	}

	if (!ReadDataEx(ucData, 1)) {
		return false;
	}

	if (!ReadDataEx(ucData + 1, ucData[0] + 1)) {
		return false;
	}

	if (!WaitData(ACK)) {
		return false;
	}

	m_product_id = MAKEWORD(ucData[0x02], ucData[0x01]);

	return true;
}

bool CPuyaISP::ExtendedErase(BYTE* pucData, BYTE ucSize)
{
	BYTE ucData[MAX_PATH] = { 0x00 };

	ucData[0x00] = m_ucEraseCmd;
	ucData[0x01] = 0xFF - ucData[0x00];
	m_pCnComm->Write(ucData, 0x02);
	if (!WaitData(ACK)) {
		return false;
	}

	memcpy(ucData, pucData, ucSize);
	ucData[ucSize] = GetXOR(ucData, ucSize, (0x01 == ucSize) ? 0xFF : 0x00);
	m_pCnComm->Write(ucData, ucSize + 1);
	if (!WaitData(ACK)) {
		return false;
	}

	return true;
}

bool CPuyaISP::WriteProtect(BYTE* pucData, BYTE ucSize)
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = CMD_WriteProtect;
	ucData[0x01] = 0xFF - CMD_WriteProtect;

	m_pCnComm->Write(ucData, 0x02);
	if (!WaitData(ACK)) {
		return false;
	}

	ucData[0x00] = ucSize - 1;
	memcpy(&ucData[0x01], pucData, ucSize);
	ucData[ucSize + 1] = GetXOR(ucData, ucSize + 1);
	m_pCnComm->Write(ucData, ucSize + 2);
	if (!WaitData(ACK)) {
		return false;
	}

	return true;
}

bool CPuyaISP::WriteUnprotect()
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = CMD_WriteUnprotect;
	ucData[0x01] = 0xFF - CMD_WriteUnprotect;

	m_pCnComm->Write(ucData, 0x02);
	if (!WaitData(ACK)) {
		return false;
	}

	if (!WaitData(ACK)) {
		return false;
	}

	return true;
}

bool CPuyaISP::ReadoutProtect()
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = CMD_ReadoutProtect;
	ucData[0x01] = 0xFF - CMD_ReadoutProtect;

	m_pCnComm->Write(ucData, 0x02);
	if (!WaitData(ACK)) {
		return false;
	}

	if (!WaitData(ACK)) {
		return false;
	}

	return true;
}

bool CPuyaISP::ReadoutUnprotect()
{
	BYTE ucData[MAX_PATH];

	ucData[0x00] = CMD_ReadoutUnprotect;
	ucData[0x01] = 0xFF - CMD_ReadoutUnprotect;

	m_pCnComm->Write(ucData, 0x02);
	if (!WaitData(ACK)) {
		return false;
	}

	if (!WaitData(ACK)) {
		return false;
	}

	return true;
}

void CPuyaISP::SetDtrAndRts(bool bSetBoot0)
{
	//BOOT0
	if (bSetBoot0)
	{
		switch (m_ucDtrRts)
		{
		case 2:
		case 5:
			m_pCnComm->Escape(CLRRTS);
			break;
		case 3:
		case 6:
			m_pCnComm->Escape(SETRTS);
			break;
		case 8:
		case 11:
			m_pCnComm->Escape(CLRDTR);
			break;
		case 9:
		case 12:
			m_pCnComm->Escape(SETDTR);
			break;
		}
	}
	else
	{
		switch (m_ucDtrRts)
		{
		case 2:
		case 5:
			m_pCnComm->Escape(SETRTS);
			break;
		case 3:
		case 6:
			m_pCnComm->Escape(CLRRTS);
			break;
		case 8:
		case 11:
			m_pCnComm->Escape(SETDTR);
			break;
		case 9:
		case 12:
			m_pCnComm->Escape(CLRDTR);
			break;
		}
	}

	//RST 为了产生下降沿
	switch (m_ucDtrRts)
	{
	case 1:
	case 2:
	case 3:
		m_pCnComm->Escape(SETDTR);
		break;
	case 4:
	case 5:
	case 6:
		m_pCnComm->Escape(CLRDTR);
		break;
	case 7:
	case 8:
	case 9:
		m_pCnComm->Escape(SETRTS);
		break;
	case 10:
	case 11:
	case 12:
		m_pCnComm->Escape(CLRRTS);
		break;
	}
	if (0x00 != m_ucDtrRts)
	{
		Sleep(1);
	}
	//RST
	switch (m_ucDtrRts)
	{
	case 1:
	case 2:
	case 3:
		m_pCnComm->Escape(CLRDTR);
		break;
	case 4:
	case 5:
	case 6:
		m_pCnComm->Escape(SETDTR);
		break;
	case 7:
	case 8:
	case 9:
		m_pCnComm->Escape(CLRRTS);
		break;
	case 10:
	case 11:
	case 12:
		m_pCnComm->Escape(SETRTS);
		break;
	}

	if (0x00 != m_ucDtrRts)
	{
		Sleep(100);
	}

	//RST
	switch (m_ucDtrRts)
	{
	case 1:
	case 2:
	case 3:
		m_pCnComm->Escape(SETDTR);
		break;
	case 4:
	case 5:
	case 6:
		m_pCnComm->Escape(CLRDTR);
		break;
	case 7:
	case 8:
	case 9:
		m_pCnComm->Escape(SETRTS);
		break;
	case 10:
	case 11:
	case 12:
		m_pCnComm->Escape(CLRRTS);
		break;
	}
}


