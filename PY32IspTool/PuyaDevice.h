#pragma once

#include <stdint.h>
#include "CnComm.h"
//#include "libusb.h"
#include "xxx_check.h"
#include "data_handle_macro.h"
#include "error.h"

#define ACK									0x79
#define NACK								0x1F

#define VID		0xFFFF
#define PID		0xFFFF

#define USBD_HID_EP_INTIN           1
#define USBD_HID_EP_INTOUT          2

#define USB_READ	(USBD_HID_EP_INTIN|LIBUSB_ENDPOINT_IN)
#define USB_WRITE	(USBD_HID_EP_INTOUT|LIBUSB_ENDPOINT_OUT)

typedef void (*_DevicePrintf)(const TCHAR* __restrict /*format*/, ...); 

class CPuyaDevice
{
public:
	CPuyaDevice();
	~CPuyaDevice();

public:
	bool ReadDataEx(BYTE* pBuffer, DWORD dwLength, DWORD dwWaitTime = 20);

	//int usb_get_device_list(uint16_t vendor_id = VID, uint16_t product_id = PID);
	//int usb_open(int interface_number, int dev_list_index = 0);
	//int usb_bulk_transfer(unsigned char endpoint, unsigned char* data, int length, unsigned int timeout = 1000);
	//int usb_interrupt_transfer(unsigned char endpoint, unsigned char* data, int length, unsigned int timeout = 1000);
	//void usb_close(void);

	virtual uint32_t Init(uint32_t nPort, uint32_t nSpeed);
	virtual uint32_t Uninit(void);
	virtual uint32_t WriteMemory(uint32_t addr, uint8_t* data, uint32_t size);
	virtual uint8_t  ReadMemory(uint32_t addr, uint8_t* data, uint32_t size);
	virtual uint32_t ErasePage(uint8_t* data, uint32_t size);
	virtual uint32_t EraseSector(uint8_t* data, uint32_t size);
	virtual uint32_t EraseChip(void);
	virtual uint8_t  Go(uint32_t addr);
	//virtual uint32_t WriteOptionBytes(uint32_t addr, uint8_t* data, uint32_t size);

	void DevicePrintf(const TCHAR* __restrict /*format*/, ...);

public:
	bool m_bRun;//如果想终止程序运行，设置此值为false

	CnComm* m_pCnComm;
	uint8_t m_ucDtrRts;

	_DevicePrintf m_pDevicePrintf;

	int m_interface_number;
	WORD m_vendor_id;
	WORD m_product_id;
	struct libusb_device_handle* m_dev_handle;
};

