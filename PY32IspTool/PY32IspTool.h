
// PY32IspTool.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CPY32IspToolApp:
// See PY32IspTool.cpp for the implementation of this class
//

class CPY32IspToolApp : public CWinApp
{
public:
	CPY32IspToolApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CPY32IspToolApp theApp;
