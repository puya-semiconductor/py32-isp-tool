#include "pch.h"
#include "HcComboBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CHcComboBox::CHcComboBox()
{
	
}

CHcComboBox::~CHcComboBox()
{
}


BEGIN_MESSAGE_MAP(CHcComboBox, CComboBox)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()


BOOL CHcComboBox::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	if ( HIWORD(wParam) == EN_CHANGE )
	{
		ShowDropDown(TRUE);
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));
		
		CString windowtext;
		GetWindowText(windowtext);

		if ( windowtext.GetLength()==0)
		{
			SetCurSel(-1);
			return true;
		}
		
		DWORD dwCurSel = GetEditSel();
		WORD start = LOWORD(dwCurSel);
		WORD end   = HIWORD(dwCurSel);
						
		CString ItemText;
		CString TempWindowText;
		int FindItem;
		int bestindex = -1;
		int bestfrom  = INT_MAX;

		for ( int x = 0; x < GetCount(); x++ )
		{
			GetLBText(x,ItemText);		
			FindItem = ItemText.Find(windowtext);
			if(FindItem==-1)
			{
				ItemText.MakeUpper();	
				TempWindowText=windowtext;
				TempWindowText.MakeUpper();
				FindItem = ItemText.Find(TempWindowText);
			}
			
			if ( FindItem != -1 && FindItem < bestfrom )
			{
				bestindex = x;
				bestfrom  = FindItem;
			}
		}

		if ( bestindex==-1)
			SetCurSel(-1);	
		else if (GetCurSel() != bestindex)
		{
			SetCurSel(bestindex);
		}
		SetWindowText(windowtext);
		SetEditSel(start,end);
		
		return true;
	}	
	return CComboBox::OnCommand(wParam, lParam);
}


BOOL CHcComboBox::OnMouseWheel(UINT /*nFlags*/, short /*zDelta*/, CPoint /*pt*/)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	//return CComboBox::OnMouseWheel(nFlags, zDelta, pt);
	return TRUE;
}
