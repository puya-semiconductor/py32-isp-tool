#pragma once

#define VALUE_UINT16(VAR, ADDR)	(*(PUINT16)(&VAR[ADDR]))
#define VALUE_UINT32(VAR, ADDR)	(*(PUINT32)(&VAR[ADDR]))
#define VALUE_UINT64(VAR, ADDR)	(*(PUINT64)(&VAR[ADDR]))

typedef struct _HexLineStruct
{
	BYTE ucDataLength;
	WORD wDataAddr;
	BYTE ucDataType;
	BYTE ucData[256];
}HexLineStruct;

class CMemoryFile
{
public:
	BOOL HexLineText2HexLineStruct();
	void HexLineStruct2HexLineText();
	void Data2HexLine(CString& text, BYTE* data, DWORD size);

	BOOL OpenHexFile(CString path, BYTE* data, DWORD& addr, DWORD& size);
	BOOL SaveHexFile(CString path, BYTE* data, DWORD addr, DWORD size);

	BOOL OpenMemoryFile(CString path, BYTE* data, DWORD& addr, DWORD& size);
	BOOL SaveMemoryFile(CString path, BYTE* data, DWORD addr, DWORD size);
	
	BOOL OpenMemoryFileEx(BYTE* data, DWORD& addr, DWORD& size);
	BOOL SaveMemoryFileEx(BYTE* data, DWORD addr, DWORD size);

	CString m_csHexLineText;
	HexLineStruct m_tHexLineStruct;
};


