#include "pch.h"
#include "MemoryFile.h"

BOOL CMemoryFile::HexLineText2HexLineStruct(void)
{
	CString csText;
	BYTE ucSum = 0x00;
	BYTE ucData[1 + 2 + 1 + 256 + 1];
	m_csHexLineText.Replace(_T(":"), _T(""));
	for (int i = 0; i < m_csHexLineText.GetLength() / 2; i++)
	{
		csText = m_csHexLineText.Mid(i * 2, 2);
		ucData[i] = (BYTE)_tcstoul(csText, NULL, 16);
		ucSum += ucData[i];
	}
	if (0x00 != ucSum)
	{
		return FALSE;
	}
	m_tHexLineStruct.ucDataLength = ucData[0x00];
	m_tHexLineStruct.wDataAddr = (ucData[0x01] << 8) + ucData[0x02];
	m_tHexLineStruct.ucDataType = ucData[0x03];
	for (int i = 0; i < ucData[0x00]; i++)
	{
		m_tHexLineStruct.ucData[i] = ucData[0x04 + i];
	}
	return TRUE;
}

void CMemoryFile::HexLineStruct2HexLineText(void)
{
	BYTE* data;
	BYTE ucSum = 0x00;

	data = new BYTE[4 + m_tHexLineStruct.ucDataLength];

	data[0x00] = m_tHexLineStruct.ucDataLength;
	data[0x01] = (m_tHexLineStruct.wDataAddr >> 8) & 0xFF;
	data[0x02] = (m_tHexLineStruct.wDataAddr >> 0) & 0xFF;
	data[0x03] = m_tHexLineStruct.ucDataType;
	for (int i = 0; i < 4; i++)
	{
		ucSum += data[i];
	}
	for (int i = 0; i < m_tHexLineStruct.ucDataLength; i++)
	{
		data[0x04+i] = m_tHexLineStruct.ucData[i];
		ucSum += m_tHexLineStruct.ucData[i];
	}
	data[0x04+ m_tHexLineStruct.ucDataLength] = 0x100 - ucSum;

	m_csHexLineText = _T(":");
	for (BYTE i = 0; i < 0x04 + m_tHexLineStruct.ucDataLength + 0x01; i++)
	{
		m_csHexLineText.AppendFormat(_T("%02X"), data[i]);
	}
	m_csHexLineText += _T("\n");

	delete []data;
}

BOOL CMemoryFile::OpenHexFile(CString path, BYTE* data, DWORD& addr, DWORD& size)
{
	CStdioFile File;
	DWORD mask, dwEndAddr, dwExtendedLinearAddress = 0;

	dwEndAddr = 0x00000000;
	mask = 0x00FFFFFF;

	if (!File.Open(path, CFile::modeRead)) {
		return FALSE;
	}

	while (File.ReadString(m_csHexLineText))
	{
		if (m_csHexLineText.IsEmpty()) {
			continue;
		}
		m_csHexLineText.Trim();
		if (!HexLineText2HexLineStruct())
		{
			File.Close();
			return FALSE;
		}
		switch (m_tHexLineStruct.ucDataType)
		{
		case 0://Data Record//数据记录
			if (dwEndAddr < (dwExtendedLinearAddress + m_tHexLineStruct.wDataAddr + m_tHexLineStruct.ucDataLength - 1)) {
				dwEndAddr = dwExtendedLinearAddress + m_tHexLineStruct.wDataAddr + m_tHexLineStruct.ucDataLength - 1;
			}			
			memcpy(&data[(dwExtendedLinearAddress & mask) + m_tHexLineStruct.wDataAddr], m_tHexLineStruct.ucData, m_tHexLineStruct.ucDataLength);
			break;
		case 1://End of File Record//文件结束记录
			break;
		case 2://Extended Segment Address Record//扩展段地址记录
			break;
		case 3://Start Segment Address Record//开始段地址记录
			break;
		case 4://Extended Linear Address Record//扩展线性地址记录			
			dwExtendedLinearAddress = ((m_tHexLineStruct.ucData[0x00] << 8) + m_tHexLineStruct.ucData[0x01]) << 16;
			if ((0 == addr) || (addr > dwExtendedLinearAddress)) {
				addr = dwExtendedLinearAddress;
			}
			if (0x1FFF0000 == (dwExtendedLinearAddress&0x1FFF0000)) {
				mask = 0xFFFF;
			}
			break;
		case 5://Start Linear Address Record//开始线性地址记录
			break;
		}
	}

	size = dwEndAddr - addr + 1;
	addr = addr & (~mask);

	File.Close();

	return TRUE;	
}

void CMemoryFile::Data2HexLine(CString& text, BYTE* data, DWORD size)
{
	BYTE ucSum = 0x00;
	for (BYTE i = 0; i < size; i++)
	{
		ucSum += data[i];
	}
	data[size] = 0x100 - ucSum;
	text = _T(":");
	for (BYTE i = 0; i < size + 1; i++)
	{
		text.AppendFormat(_T("%02X"), data[i]);
	}
	text += _T("\n");
}

BOOL CMemoryFile::SaveHexFile(CString path, BYTE* data, DWORD addr, DWORD size)
{
	CString csText;
	DWORD mask;
	BYTE ucData[0x25];
	CStdioFile StdioFile;

	mask = ~addr;

	path.Replace(_T(".hex"), _T(""));

	if (StdioFile.Open(path + _T(".hex"), CFile::modeWrite | CFile::modeCreate) == NULL)
	{
		return FALSE;
	}

	for (DWORD i = 0; i < size / 0x10000; i++)
	{
		ucData[0x00] = 0x02;
		ucData[0x01] = 0x00;
		ucData[0x02] = 0x00;
		ucData[0x03] = 0x04;
		ucData[0x04] = (addr >> 24) & 0xFF;
		ucData[0x05] = (addr >> 16) & 0xFF;
		Data2HexLine(csText, ucData, 0x06);
		StdioFile.WriteString(csText);
		for (DWORD j = 0; j < 0x10000 / 0x20; j++)
		{
			ucData[0x00] = 0x20;
			ucData[0x01] = (addr >> 8) & 0xFF;
			ucData[0x02] = (addr >> 0) & 0xFF;
			ucData[0x03] = 0x00;
			memcpy(&ucData[0x04], &data[addr & mask], 0x20);
			Data2HexLine(csText, ucData, 0x24);
			StdioFile.WriteString(csText);
			addr += 0x20;
		}
	}
	if (size % 0x10000)
	{
		ucData[0x00] = 0x02;
		ucData[0x01] = 0x00;
		ucData[0x02] = 0x00;
		ucData[0x03] = 0x04;
		ucData[0x04] = (addr >> 24) & 0xFF;
		ucData[0x05] = (addr >> 16) & 0xFF;
		Data2HexLine(csText, ucData, 0x06);
		StdioFile.WriteString(csText);
		for (DWORD j = 0; j < (size % 0x10000) / 0x20; j++)
		{
			ucData[0x00] = 0x20;
			ucData[0x01] = (addr >> 8) & 0xFF;
			ucData[0x02] = (addr >> 0) & 0xFF;
			ucData[0x03] = 0x00;
			memcpy(&ucData[0x04], &data[addr & mask], 0x20);
			Data2HexLine(csText, ucData, 0x24);
			StdioFile.WriteString(csText);
			addr += 0x20;
		}
		if (size % 0x20)
		{
			ucData[0x00] = (size % 0x20);
			ucData[0x01] = (addr >> 8) & 0xFF;
			ucData[0x02] = (addr >> 0) & 0xFF;
			ucData[0x03] = 0x00;
			memcpy(&ucData[0x04], &data[addr & mask], (size % 0x20));
			Data2HexLine(csText, ucData, (size % 0x20)+4);
			StdioFile.WriteString(csText);
			addr += (size % 0x20);
		}
	}
	StdioFile.WriteString(_T(":00000001FF\n"));
	StdioFile.Close();

	return TRUE;
}

BOOL CMemoryFile::OpenMemoryFile(CString path, BYTE* data, DWORD& addr, DWORD& size)
{
	CString csFile;
	DWORD dwStackBase, dwResetBase, dwResetOffset;

	if (!PathFileExists(path))
	{
		return FALSE;
	}

	csFile = path;

	csFile.MakeLower();

	if (_T(".bin") == csFile.Right(4))
	{
		CFile File;	

		if (!File.Open(csFile, CFile::modeRead, NULL))
		{
			return FALSE;
		}	

		size = File.Read(data, (UINT)File.GetLength());

		File.Close();

		dwStackBase = VALUE_UINT32(data, 0) & 0xFF000000;
		dwResetBase = VALUE_UINT32(data, 4) & 0xFF000000;
		dwResetOffset = VALUE_UINT32(data, 4) & 0x00FFFF00;

		if ((0x20000000 != dwStackBase) || (0x08000000 != dwResetBase)) {
			return TRUE;
		}

		addr = dwResetBase;
		memset(data, 0xFF, size);

		if (!File.Open(csFile, CFile::modeRead, NULL))
		{
			return FALSE;
		}

		size = File.Read(data + dwResetOffset, (UINT)File.GetLength());

		File.Close();
	}

	if (_T(".hex") == csFile.Right(4))
	{
		return OpenHexFile(csFile, data, addr, size);
	}

	return TRUE;
}

BOOL CMemoryFile::OpenMemoryFileEx(BYTE* data, DWORD& addr, DWORD& size)
{
	CString csFile;

	CFileDialog FileDialog(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("Hex/Bin Files(*.hex;*.bin)|*.hex;*.bin||"), NULL);

	if (IDOK != FileDialog.DoModal())
	{
		return FALSE;
	}

	csFile = FileDialog.GetPathName();

	return OpenMemoryFile(csFile, data, addr, size);
}

BOOL CMemoryFile::SaveMemoryFile(CString path, BYTE* data, DWORD addr, DWORD size)
{
	CString csFile;

	csFile = path;
	
	csFile.MakeLower();

	if (_T(".bin") == csFile.Right(4))
	{
		CFile File;

		csFile.Replace(_T(".bin"), _T(""));

		if (!File.Open(csFile + _T(".bin"), CFile::modeWrite | CFile::modeCreate, NULL))
		{
			return FALSE;
		}

		File.Write(data, size);

		File.Close();
	}

	if (_T(".hex") == csFile.Right(4))
	{
		return SaveHexFile(csFile, data, addr, size);
	}
	
	return TRUE;
}

BOOL CMemoryFile::SaveMemoryFileEx(BYTE* data, DWORD addr, DWORD size)
{
	CString csFile;

	CFileDialog FileDialog(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_NOCHANGEDIR, _T("Bin File(*.bin)|*.bin|Hex File(*.hex)|*.hex||"), NULL);

	if (IDOK != FileDialog.DoModal())
	{
		return FALSE;
	}

	csFile = FileDialog.GetPathName();

	if (1 == FileDialog.m_ofn.nFilterIndex)
	{
		csFile += _T(".bin");
	}

	if (2 == FileDialog.m_ofn.nFilterIndex)
	{
		csFile += _T(".hex");
	}
	
	return SaveMemoryFile(csFile, data, addr, size);
}