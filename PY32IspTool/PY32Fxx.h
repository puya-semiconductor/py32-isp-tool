#ifndef __PY_PROGRAM_PROTOCOL_H
#define __PY_PROGRAM_PROTOCOL_H

typedef struct
{ 
    DWORD ID;                       /* 0x00000000 文件标识 */
    BYTE  NAME[0x40];               /* 0x00000004 文件注释 */
    DWORD DL_LIMIT;                 /* 0x00000044 下载限制次数 */
    DWORD NUM;                      /* 0x00000048 文件编号 */
    DWORD VERSION;                  /* 0x0000004C 文件版本 */
    BYTE  CUSTOMER[0x20];           /* 0x00000050 客户名称 */
    DWORD PRO_LIMIT;                /* 0x00000070 烧录限定次数 */
    BYTE  REDUCE_MODE;              /* 0x00000074 减数方式 */
    DWORD EDIT_PSW;                 /* 0x00000075 镜像编辑密码 */
    DWORD PROGRAM_PSW[2];           /* 0x00000079 镜像启动密码 */
    DWORD READ_PSW[2];              /* 0x00000081 镜像读取密码 */
    DWORD CODE_SIZE;                /* 0x00000089 程序大小 */
    DWORD START_ADDR;               /* 0x0000008D 下载起始地址 */
    DWORD DATA_ADDR;                /* 0x00000091 运行起始地址 */
    BYTE  RESERVE1[0x6B];           /* 0x00000095 保留 */

    BYTE  BURST_MODE;               /* 0x00000100 触发方式 */
    BYTE  OUT_VOLTAGE;              /* 0x00000101 输出电压 */
    WORD  PRE_DELAY;                /* 0x00000102 烧录前延时 */
    BYTE  PRO_MODE;                 /* 0x00000104 接口 */
    DWORD PRO_SPEED;                /* 0x00000105 烧录速度 */
    DWORD BAUD_RATE;                /* 0x00000109 串口波特率 */
    BYTE  RECORD_UID;               /* 0x0000010D 是否记录UID */
    BYTE  CHECK_UID;                /* 0x0000010E 检查绑定UID */
    DWORD CHECK_UID_COUNT;          /* 0x0000010F 绑定UID的个数 */
    BYTE  ERASE_MODE;               /* 0x00000113 擦除模式 */
    BYTE  BLANK_FLASH;              /* 0x00000114 查空FLASH */
    BYTE  PROGRAM_FLASH;            /* 0x00000115 写FLASH */
    BYTE  VERIFY_FLASH;             /* 0x00000116 校FLASH */
    BYTE  PROGRAM_EEPROM;           /* 0x00000117 写EEPROM */
    BYTE  VERIFY_EEPROM;            /* 0x00000118 校EEPROM */
    BYTE  PROGRAM_OTP;              /* 0x00000119 写OTP */
    BYTE  VERIFY_OTP;               /* 0x0000011A 校OTP */
    BYTE  UID_ENCRYPT;              /* 0x0000011B UID自定义算法加密 */
    BYTE  PROGRAM_ROLL_CODE;        /* 0x0000011C 写滚码 */
    BYTE  PROGRAM_BAR_CODE;         /* 0x0000011D 写条码 */
    BYTE  PROGRAM_OPTION;           /* 0x0000011E 写并校验选项字节 */
    BYTE  DONE_OPERATE;             /* 0x0000011F 尾部操作 */
    BYTE  GO_MODE;                  /* 0x00000120 跳转模式 */
    WORD  DONE_DELAY;               /* 0x00000121 烧录完成延时 */
    DWORD GO2NEXT_FILE;             /* 0x00000123 跳转镜像号 */
    DWORD VERIFY_ALGO_CONFIG;       /* 0x00000127 校验算法配置 */
    BYTE  RESERVE2[0x55];           /* 0x0000012B 保留 */

    DWORD OPTION_BYTES[0x80/4];     /* 0x00000180 选项字节 */

    BYTE  ROLL_CODE_MEM_TYPE;       /* 0x00000200 目标存储器类型 */
    BYTE  ROLL_CODE_START_VAR[0x20];/* 0x00000201 滚码起始值 */
    DWORD ROLL_CODE_STEP_VAR;       /* 0x00000221 滚码步进值 */
    DWORD ROLL_CODE_START_ADDR;     /* 0x00000225 滚码始地址 */
    BYTE  ROLL_CODE_BYTES;          /* 0x00000229 滚码字节数 */
    BYTE  ROLL_CODE_ENCODE_MODE;    /* 0x0000022A 编码模式 */
    BYTE  RESERVE3[0x55];           /* 0x0000022B 保留 */

    BYTE  BAR_CODE[0x80];           /* 0x00000280 条码配置 */

    DWORD UID_ENCRYPT_ADDR;         /* 0x00000300 存放起始地址 */
    BYTE  UID_ENCRYPT_BYTES;        /* 0x00000304 存储字节数 */
    DWORD UID_ENCRYPT_CONSTANT;     /* 0x00000305 输入常数 */
    BYTE  UID_ENCRYPT_ALGO;         /* 0x00000309 使用的公式 */
    BYTE  UID_ENCRYPT_D[0x0C];      /* 0x0000030A D[0-11] */
    BYTE  RESERVE4[0x6A];           /* 0x00000316 保留 */

    DWORD FLASH_CHECKSUM;           /* 0x00000380 FLASH校验和 */
    DWORD FLASH_CRC;                /* 0x00000384 FLASH CRC */
    DWORD OPTION_BYTES_MASK;        /* 0x00000388 OPTION数据掩码 */
    DWORD OPTION_BYTES_CHECKSUM;    /* 0x0000038C OPTION 校验和;/* 0x80B/4/8=4B */
    DWORD OPTION_BYTES_CRC;         /* 0x00000390 OPTION CRC */
    DWORD EEPROM_MASK[0x02];        /* 0x00000394 EEPROM数据掩码8KB/;/* 0x80/8=8B */
    DWORD EEPROM_CHECKSUM;          /* 0x0000039C EEPROM 校验和 */
    DWORD EEPROM_CRC;               /* 0x000003A0 EEPROM CRC */
    DWORD OTP_MASK[0x02];           /* 0x000003A4 OTP数据掩码 8KB/;/* 0x80/8=8B */
    DWORD OTP_CHECKSUM;             /* 0x000003AC OTP校验和 */
    DWORD OTP_CRC;                  /* 0x000003B0 OTP CRC */
    BYTE  RESERVE5[0x44];           /* 0x000003B4 保留 */
    DWORD CHECKSUM;                 /* 0x000003F8 脱机镜像文件校验和 */
    DWORD CRC;                      /* 0x000003FC 脱机镜像文件CRC */
}FILE_TypeDef;

typedef struct
{
    BYTE  VENDOR[0x20];             /* 0x00000400 厂商名称 */
    BYTE  SERIES[0x20];             /* 0x00000420 芯片系列 */
    BYTE  NAME[0x20];               /* 0x00000440 芯片型号 */
    DWORD FLASH_ADDR;               /* 0x00000460 FLASH地址 */
    DWORD FLASH_SIZE;               /* 0x00000464 FLASH大小 */
    DWORD PAGE_SIZE;                /* 0x00000468 页大小 */
    DWORD SECTOR_SIZE;              /* 0x0000046C 扇区大小 */
    DWORD BLOCK_SIZE1;              /* 0x00000470 块大小1 */
    DWORD BLOCK_SIZE2;              /* 0x00000474 块大小2 */
    DWORD SYSTEM_ADDR;              /* 0x00000478 SYSTEM地址 */
    DWORD SYSTEM_SIZE;              /* 0x0000047C SYSTEM大小 */
    DWORD UID_ADDR;                 /* 0x00000480 UID地址 */
    DWORD UID_SIZE;                 /* 0x00000484 UID大小 */
    DWORD OPTION_ADDR;              /* 0x00000488 OPTION地址 */
    DWORD OPTION_SIZE;              /* 0x0000048C OPTION大小 */
    DWORD FACTORY_ADDR;             /* 0x00000490 Factory config.bytes地址 */
    DWORD FACTORY_SIZE;             /* 0x00000494 Factory config.bytes大小 */
    DWORD RAM_ADDR;                 /* 0x00000498 RAM地址 */
    DWORD RAM_SIZE;                 /* 0x0000049C RAM大小 */
    DWORD EEPROM_ADDR;              /* 0x000004A0 EEPROM地址 */
    DWORD EEPROM_SIZE;              /* 0x000004A4 EEPROM大小 */
    DWORD OTP_ADDR;                 /* 0x000004A8 OTP地址 */
    DWORD OTP_SIZE;                 /* 0x000004AC OTP大小 */
    DWORD BOOT_VERSION;             /* 0x000004B0 自举程序版本 */
    BYTE  JTAG_SUPPORT;             /* 0x000004B4 是否支持JTAG */
    BYTE  SWD_SUPPORT;              /* 0x000004B5 是否支持SWD */
    BYTE  ISP_SUPPORT;              /* 0x000004B6 是否支持ISP */
    BYTE  RESERVE[0x345];           /* 0x000004B7 保留 */
    DWORD CRC;                      /* 0x000007FC CRC */
}CHIP_TypeDef;


#endif //__PY_PROGRAM_PROTOCOL_H