#pragma once

typedef struct _HexLineStruct
{
	BYTE ucDataLength;
	WORD wDataAddr;
	BYTE ucDataType;
	BYTE ucData[256];
}HexLineStruct;

class CParseHexFile
{
public:
	BOOL HexLineText2HexLineStruct();
	void HexLineStruct2HexLineText();

	BOOL ReadHexFile(CString csHexFileName, BYTE* pucData, DWORD& dwAddr, DWORD &dwLength);
	BOOL SaveHexFile(CString csHexFileName, BYTE* pucData, DWORD dwAddr, DWORD dwLength);

	CString m_csHexLineText;
	HexLineStruct m_tHexLineStruct;
};

//BOOL HexLineText2HexLineStruct(CString csHexLineText, HexLineStruct& tHexLineStruct);
//void HexLineStruct2HexLineText(HexLineStruct tHexLineStruct, CString& csHexLineText);


