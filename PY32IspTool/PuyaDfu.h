#pragma once

#include "PuyaDevice.h"

class CPuyaDfu :
    public CPuyaDevice
{
public:
	virtual uint32_t Init(uint32_t nPort, uint32_t nPID);
	virtual uint32_t Uninit(void);
	virtual uint32_t WriteMemory(uint32_t addr, uint8_t* data, uint32_t size);
	virtual uint8_t  ReadMemory(uint32_t addr, uint8_t* data, uint32_t size);
	virtual uint32_t ErasePage(uint8_t* data, uint32_t size);
	virtual uint32_t EraseSector(uint8_t* data, uint32_t size);
	virtual uint32_t EraseChip(void);
	virtual uint8_t  Go(uint32_t addr);
	virtual uint32_t WriteOptionBytes(uint32_t addr, uint8_t* data, uint32_t size);

private:
	bool WaitData(const uint8_t ucData);
	bool Get();
	bool GetVersionAndReadProtectionStatus();
	bool GetID();
	bool ExtendedErase(BYTE* pucData, BYTE ucSize);
	bool WriteProtect(BYTE* pucData, BYTE ucSize);
	bool WriteUnprotect(void);
	bool ReadoutProtect(void);
	bool ReadoutUnprotect(void);
};

