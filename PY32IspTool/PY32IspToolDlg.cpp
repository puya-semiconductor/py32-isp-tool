
// PY32IspToolDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "PY32IspTool.h"
#include "PY32IspToolDlg.h"
#include "afxdialogex.h"
//#include "py_program_protocol.h"
#include "MemoryFile.h"
#include "OptionBytesDlg.h"
#include <setupapi.h>
#pragma comment(lib, "setupapi.lib")
#include <objbase.h>
#include <devguid.h>
#include "CSetAddrSizeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

void CPY32IspToolDlg::SetStructData(uint32_t mcu_pid)
{
	if (m_wPID == mcu_pid) {
		return;
	}
	m_wPID = mcu_pid;

	memset(FILE_InitStruct.OPTION_BYTES, 0xFF, sizeof(FILE_InitStruct.OPTION_BYTES));

	switch (mcu_pid)
	{
	case 0x0068:
		//_tcscpy_s((TCHAR*)(CHIP_InitStruct.SERIES), 0x20, _T("PY32T020xx"));
		//_tcscpy_s((TCHAR*)(CHIP_InitStruct.NAME), 0x20, _T("PY32T020x6T6"));
		CHIP_InitStruct.FLASH_SIZE = 0x00008000;
		CHIP_InitStruct.PAGE_SIZE = 0x00000080;
		CHIP_InitStruct.SECTOR_SIZE = 0x00001000;
		CHIP_InitStruct.SYSTEM_ADDR = 0x00000000;
		CHIP_InitStruct.SYSTEM_SIZE = 0x00000000;
		CHIP_InitStruct.UID_ADDR = 0x1FFF0000;
		CHIP_InitStruct.UID_SIZE = 0x00000080;
		CHIP_InitStruct.OPTION_ADDR = 0x1FFF0080;
		CHIP_InitStruct.OPTION_SIZE = 0x00000010;
		CHIP_InitStruct.FLASHSIZE_ADDR = 0x1FFF01FC;
		CHIP_InitStruct.RAM_SIZE = 0x00001000;
		M32(&FILE_InitStruct.OPTION_BYTES[0x00]) = 0x6F5590AA;
		M32(&FILE_InitStruct.OPTION_BYTES[0x04]) = 0xFFF0000F;
		M32(&FILE_InitStruct.OPTION_BYTES[0x08]) = 0x7FFF8000;
		M32(&FILE_InitStruct.OPTION_BYTES[0x0C]) = 0xFF0000FF;
		break;
	case 0x0064:
		//_tcscpy_s((TCHAR*)(CHIP_InitStruct.SERIES), 0x20, _T("PY32F002Bxx"));
		//_tcscpy_s((TCHAR*)(CHIP_InitStruct.NAME), 0x20, _T("PY32F002Bx5T6"));
		CHIP_InitStruct.FLASH_SIZE = 0x00006000;
		CHIP_InitStruct.PAGE_SIZE = 0x00000080;
		CHIP_InitStruct.SECTOR_SIZE = 0x00001000;
		CHIP_InitStruct.SYSTEM_ADDR = 0x00000000;
		CHIP_InitStruct.SYSTEM_SIZE = 0x00000000;
		CHIP_InitStruct.UID_ADDR = 0x1FFF0000;
		CHIP_InitStruct.UID_SIZE = 0x00000080;
		CHIP_InitStruct.OPTION_ADDR = 0x1FFF0080;
		CHIP_InitStruct.OPTION_SIZE = 0x00000010;
		CHIP_InitStruct.FLASHSIZE_ADDR = 0x1FFF0088;
		CHIP_InitStruct.RAM_SIZE = 0x00000C00;
		M32(&FILE_InitStruct.OPTION_BYTES[0x00]) = 0x4F55B0AA;
		M32(&FILE_InitStruct.OPTION_BYTES[0x04]) = 0xFFF4000B;
		M32(&FILE_InitStruct.OPTION_BYTES[0x08]) = 0xFFFF0000;
		M32(&FILE_InitStruct.OPTION_BYTES[0x0C]) = 0xFFC0003F;
		break;
	case 0x0440:
		//_tcscpy_s((TCHAR*)(CHIP_InitStruct.SERIES), 0x20, _T("PY32F030xx"));
		//_tcscpy_s((TCHAR*)(CHIP_InitStruct.NAME), 0x20, _T("PY32F030x8T6"));
		CHIP_InitStruct.FLASH_SIZE = 0x00010000;
		CHIP_InitStruct.PAGE_SIZE = 0x00000080;
		CHIP_InitStruct.SECTOR_SIZE = 0x00001000;
		CHIP_InitStruct.SYSTEM_ADDR = 0x1FFF0000;
		CHIP_InitStruct.SYSTEM_SIZE = 0x00000E00;
		CHIP_InitStruct.UID_ADDR = 0x1FFF0E00;
		CHIP_InitStruct.UID_SIZE = 0x00000080;
		CHIP_InitStruct.OPTION_ADDR = 0x1FFF0E80;
		CHIP_InitStruct.OPTION_SIZE = 0x00000010;
		CHIP_InitStruct.FLASHSIZE_ADDR = 0x1FFF0FFC;
		CHIP_InitStruct.RAM_SIZE = 0x00002000;
		M32(&FILE_InitStruct.OPTION_BYTES[0x00]) = 0x4155BEAA;
		M32(&FILE_InitStruct.OPTION_BYTES[0x04]) = 0xFF0000FF;
		M32(&FILE_InitStruct.OPTION_BYTES[0x0C]) = 0x0000FFFF;
		break;
	case 0x0444:
		//_tcscpy_s((TCHAR*)(CHIP_InitStruct.SERIES), 0x20, _T("PY32F031xx"));
		//_tcscpy_s((TCHAR*)(CHIP_InitStruct.NAME), 0x20, _T("PY32F031x8T6"));
		CHIP_InitStruct.FLASH_SIZE = 0x00010000;
		CHIP_InitStruct.PAGE_SIZE = 0x00000080;
		CHIP_InitStruct.SECTOR_SIZE = 0x00001000;
		CHIP_InitStruct.SYSTEM_ADDR = 0x1FFF0000;
		CHIP_InitStruct.SYSTEM_SIZE = 0x00000E00;
		CHIP_InitStruct.UID_ADDR = 0x1FFF0E00;
		CHIP_InitStruct.UID_SIZE = 0x00000080;
		CHIP_InitStruct.OPTION_ADDR = 0x1FFF0E80;
		CHIP_InitStruct.OPTION_SIZE = 0x00000010;
		CHIP_InitStruct.FLASHSIZE_ADDR = 0x1FFF0FFC;
		CHIP_InitStruct.RAM_SIZE = 0x00002000;
		M32(&FILE_InitStruct.OPTION_BYTES[0x00]) = 0x4755B8AA;
		M32(&FILE_InitStruct.OPTION_BYTES[0x04]) = 0xFFF1000E;
		M32(&FILE_InitStruct.OPTION_BYTES[0x08]) = 0xFFE0001F;
		M32(&FILE_InitStruct.OPTION_BYTES[0x0C]) = 0x0000FFFF;
		break;
	case 0x0063:
	case 0x0448:
		//_tcscpy_s((TCHAR*)(CHIP_InitStruct.SERIES), 0x20, _T("PY32F072xx"));
		//_tcscpy_s((TCHAR*)(CHIP_InitStruct.NAME), 0x20, _T("PY32F072xBT6"));
		CHIP_InitStruct.FLASH_SIZE = 0x00020000;
		CHIP_InitStruct.PAGE_SIZE = 0x00000100;
		CHIP_InitStruct.SECTOR_SIZE = 0x00002000;
		CHIP_InitStruct.SYSTEM_ADDR = 0x1FFF0000;
		CHIP_InitStruct.SYSTEM_SIZE = 0x00003000;
		CHIP_InitStruct.UID_ADDR = 0x1FFF3000;
		CHIP_InitStruct.UID_SIZE = 0x00000100;
		CHIP_InitStruct.OPTION_ADDR = 0x1FFF3100;
		CHIP_InitStruct.OPTION_SIZE = 0x00000020;
		CHIP_InitStruct.FLASHSIZE_ADDR = 0x1FFF33F8;
		CHIP_InitStruct.RAM_SIZE = 0x00004000;
		M32(&FILE_InitStruct.OPTION_BYTES[0x00]) = 0x2755D8AA;
		M32(&FILE_InitStruct.OPTION_BYTES[0x08]) = 0xFFE0001F;
		M32(&FILE_InitStruct.OPTION_BYTES[0x18]) = 0x0000FFFF;
		break;
	case 0x0100:
	case 0x0413:
		//_tcscpy_s((TCHAR*)(CHIP_InitStruct.SERIES), 0x20, _T("PY32F403xx"));
		//_tcscpy_s((TCHAR*)(CHIP_InitStruct.NAME), 0x20, _T("PY32F403xDT6"));
		CHIP_InitStruct.FLASH_SIZE = 0x00060000;
		CHIP_InitStruct.PAGE_SIZE = 0x00000100;
		CHIP_InitStruct.SECTOR_SIZE = 0x00000800;
		CHIP_InitStruct.SYSTEM_ADDR = 0x1FFF0000;
		CHIP_InitStruct.SYSTEM_SIZE = 0x00005000;
		CHIP_InitStruct.UID_ADDR = 0x1FFF5800;
		CHIP_InitStruct.UID_SIZE = 0x00000080;
		CHIP_InitStruct.OPTION_ADDR = 0x1FFF5000;
		CHIP_InitStruct.OPTION_SIZE = 0x00000010;
		CHIP_InitStruct.FLASHSIZE_ADDR = 0x1FFF5700;
		CHIP_InitStruct.RAM_SIZE = 0x00008000;
		M32(&FILE_InitStruct.OPTION_BYTES[0x00]) = 0x8F5570AA;
		M32(&FILE_InitStruct.OPTION_BYTES[0x0C]) = 0xF0000FFF;
		break;
	}
	_tcscpy_s((TCHAR*)(CHIP_InitStruct.VENDOR), 0x20, _T("Puya Semiconductor"));
	
	CHIP_InitStruct.FLASH_ADDR = 0x08000000;	
	CHIP_InitStruct.RAM_ADDR = 0x20000000;
	CHIP_InitStruct.SWD_SUPPORT = 1;
	CHIP_InitStruct.ISP_SUPPORT = 1;
	CHIP_InitStruct.CRC = GetCrc16((const BYTE*)(&CHIP_InitStruct.VENDOR), 0x400);

	FILE_InitStruct.BASE_ADDR = CHIP_InitStruct.FLASH_ADDR;
}

#define MAX_MEMORY_SIZE (512*0x400)

// CPY32IspToolDlg dialog
CPY32IspToolDlg::CPY32IspToolDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PY32ISPTOOL_DIALOG, pParent)
	, m_pDevice(NULL)
	, m_nRadioErase(0)
	, m_wPID(0)
	, m_dwComPort(0)
	, m_bReadMemoryOneTime(TRUE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);	

	SetStructData(0x0448);
	//FILE_InitStruct.ID = (CTime::GetCurrentTime().GetTime() & 0xFFFFFFFF);
	FILE_InitStruct.PRO_MODE = PRO_ISP;
	FILE_InitStruct.PRO_SPEED = 1000000;
	FILE_InitStruct.BAUD_RATE = AfxGetApp()->GetProfileInt(_T("Settings"), _T("BaudRate"), 115200);
	FILE_InitStruct.ERASE_MODE = 2;
	FILE_InitStruct.PROGRAM_FLASH = 1;
	FILE_InitStruct.VERIFY_FLASH = 1;
	FILE_InitStruct.DONE_OPERATE = 0;
	FILE_InitStruct.DTR_RTS = AfxGetApp()->GetProfileInt(_T("Settings"), _T("DtrRts"), 0);
	FILE_InitStruct.CODE_SIZE = 0x00000000;

	m_pucProgramMemory = new BYTE[MAX_MEMORY_SIZE];
	m_pucTargetMemory = new BYTE[MAX_MEMORY_SIZE];
	
	memset(m_pucProgramMemory, 0xFF, MAX_MEMORY_SIZE);
	memset(m_pucTargetMemory, 0xFF, MAX_MEMORY_SIZE);

	TRACE(_T("sizeof(FILE_InitStruct) = 0x%04X, sizeof(CHIP_InitStruct) = 0x%04X.\n"), sizeof(FILE_InitStruct), sizeof(CHIP_InitStruct));
}

CPY32IspToolDlg::~CPY32IspToolDlg()
{
	delete[] m_pucProgramMemory;
	delete[] m_pucTargetMemory;
}

void CPY32IspToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS, m_XProgressCtrl);
	DDX_Control(pDX, IDC_EDIT_LOG, m_EditLog);
	DDX_Radio(pDX, IDC_RADIO_DONOT_ERASE, m_nRadioErase);
	DDX_Control(pDX, IDC_COMBO_ISP_DEVICE, m_ComboBoxIspDevice);
	DDX_Control(pDX, IDC_COMBO_BAUDRATE, m_ComboBoxBaudrate);
	DDX_Control(pDX, IDC_COMBO_DTR_RTS, m_HcComboBoxDtrRts);
}

BEGIN_MESSAGE_MAP(CPY32IspToolDlg, CDialogEx)
	ON_WM_DEVICECHANGE()
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_CHECK_PROGRAM, &CPY32IspToolDlg::OnBnClickedCheckProgram)
	ON_BN_CLICKED(IDC_CHECK_VERIFY, &CPY32IspToolDlg::OnBnClickedCheckVerify)
	ON_BN_CLICKED(IDC_CHECK_OPTION_BYTES, &CPY32IspToolDlg::OnBnClickedCheckProgramOptionBytes)
	ON_BN_CLICKED(IDC_CHECK_RUN_APP, &CPY32IspToolDlg::OnBnClickedCheckRunApp)
	ON_BN_CLICKED(IDC_BUTTON_DNLOAD_OPEN, &CPY32IspToolDlg::OnBnClickedButtonDnloadOpen)
	ON_BN_CLICKED(IDC_BUTTON_RUN_APP, &CPY32IspToolDlg::OnBnClickedButtonRunApp)
	ON_BN_CLICKED(IDC_BUTTON_UPLOAD, &CPY32IspToolDlg::OnBnClickedButtonUpload)
	ON_BN_CLICKED(IDC_BUTTON_DNLOAD, &CPY32IspToolDlg::OnBnClickedButtonDnload)
	ON_CBN_SELCHANGE(IDC_COMBO_ISP_DEVICE, &CPY32IspToolDlg::OnCbnSelchangeComboIspDevice)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &CPY32IspToolDlg::OnBnClickedButtonConnect)
	ON_CBN_SELCHANGE(IDC_COMBO_BAUDRATE, &CPY32IspToolDlg::OnCbnSelchangeComboBaudrate)
	ON_CBN_SELCHANGE(IDC_COMBO_DTR_RTS, &CPY32IspToolDlg::OnCbnSelchangeComboDtrRts)
END_MESSAGE_MAP()


// CPY32IspToolDlg message handlers

BOOL CPY32IspToolDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CString csWindowText;
	m_Font.CreatePointFont(100, _T("Consolas"));
	GetDlgItem(IDC_EDIT_LOG)->SetFont(&m_Font);
	GetDlgItem(IDC_COMBO_APP_ADDR)->SetFont(&m_Font);

	m_nRadioErase = FILE_InitStruct.ERASE_MODE;

	m_ComboBoxBaudrate.SetCurSel(9);

	((CButton*)GetDlgItem(IDC_CHECK_PROGRAM))->SetCheck(FILE_InitStruct.PROGRAM_FLASH);
	((CButton*)GetDlgItem(IDC_CHECK_VERIFY))->SetCheck(FILE_InitStruct.VERIFY_FLASH);
	((CButton*)GetDlgItem(IDC_CHECK_PROGRAM_OPTION_BYTES))->SetCheck(FILE_InitStruct.PROGRAM_OPTION);
	((CButton*)GetDlgItem(IDC_CHECK_RUN_APP))->SetCheck(FILE_InitStruct.DONE_OPERATE);

	m_HcComboBoxDtrRts.ResetContent();
	for (int i = 0; i < 13; i++)
	{
		m_HcComboBoxDtrRts.InsertString(i, DTR_RTS_STR[i]);
	}
	m_HcComboBoxDtrRts.SetCurSel(FILE_InitStruct.DTR_RTS);

	for (int i = 0; i < 11; i++)
	{
		csWindowText.Format(_T("%d"), BAUD_RATE[i]);
		m_ComboBoxBaudrate.InsertString(i, csWindowText);
		if (BAUD_RATE[i] == FILE_InitStruct.BAUD_RATE)
		{
			m_ComboBoxBaudrate.SetCurSel(i);
		}
	}

	OnDeviceChange(0, 0);

	UpdateData(FALSE);

	EnableCtrl(FALSE);

	//UpdateStatus(_T("The following are supported MCUs and pins:\r\n "));
	//UpdateStatus(_T("(1)PY32F030/PY32F003/PY32F002A:\r\nTX/RX: PA2/PA3 PA9/PA10 PA14/PA15\r\n"));
	//UpdateStatus(_T("(2)PY32F072/PY32F071/PY32F040:\r\nTX/RX: PA9/PA10 PA14/PA15\r\n"));
	//UpdateStatus(_T("(3)PY32F303/PY32F403:\r\nTX/RX: PA9/PA10 PB10/PB11 PC10/PC11 PD5/PD6\r\n"));
	//UpdateStatus(_T("(3)PY32F002B/PY32L020:\r\nTX/RX: PA3/PA4\r\n"));

	if (PathFileExists(AfxGetApp()->m_lpCmdLine))
	{
		m_bExitApp = TRUE;
		OpenFile(AfxGetApp()->m_lpCmdLine);
		OnBnClickedButtonConnect();
		OnBnClickedButtonDnload();
	}
	else
	{
		m_bExitApp = FALSE;
		//OpenFile(AfxGetApp()->GetProfileString(_T("Settings"), _T("FileName")));
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPY32IspToolDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPY32IspToolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPY32IspToolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

#define MAX_MCU_SUPPORT 2

const uint16_t MCU_PID[MAX_MCU_SUPPORT] = { 0x0448, 0x0413 };
const CString MCU_STR[MAX_MCU_SUPPORT] = { _T("PY32F072"), _T("PY32F403") };

BOOL CPY32IspToolDlg::OnDeviceChange(UINT /*nEventType*/, DWORD_PTR /*dwData*/)
{
	DWORD dwCurPort = 0;
	DWORD dwLastPort = 0;
	UINT  nCount = 0;
	DWORD dwDataType;
	DWORD dwBufSize;

	TCHAR szBuffer[MAX_PATH];

	CString csBuffer;

	HDEVINFO hDevInfo;

	m_ComboBoxIspDevice.ResetContent();

	dwLastPort = AfxGetApp()->GetProfileInt(_T("Settings"), _T("ComPort"), 0);
	hDevInfo = SetupDiGetClassDevs((LPGUID)&GUID_DEVCLASS_PORTS, 0, 0, DIGCF_PRESENT);

	if (INVALID_HANDLE_VALUE != hDevInfo)
	{
		SP_DEVINFO_DATA DeviceInfoData = { sizeof(SP_DEVINFO_DATA) };
		for (DWORD i = 0; SetupDiEnumDeviceInfo(hDevInfo, i, &DeviceInfoData); i++)
		{
			memset(szBuffer, 0, sizeof(szBuffer));
			dwBufSize = sizeof(szBuffer);
			if (SetupDiGetDeviceRegistryProperty(hDevInfo, &DeviceInfoData, SPDRP_FRIENDLYNAME, &dwDataType, (PBYTE)szBuffer, dwBufSize, &dwBufSize))
			{
				csBuffer.Format(_T("%s"), szBuffer);
				if (-1 == csBuffer.Find(_T("通信端口")))
				{
					csBuffer = csBuffer.Right(csBuffer.GetLength() - csBuffer.Find(_T("(COM")) - 4);
					csBuffer.Replace(_T(")"), _T(""));
					dwCurPort = _tcstoul(csBuffer, NULL, 10);
					csBuffer.Format(_T("COM%d"), dwCurPort);
					m_ComboBoxIspDevice.InsertString(nCount++, szBuffer);
					if (dwLastPort == dwCurPort)
					{
						m_ComboBoxIspDevice.SetCurSel(nCount - 1);
					}
				}
			}
		}
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	if (m_ComboBoxIspDevice.GetCount() > 0 && 0 > m_ComboBoxIspDevice.GetCurSel())
	{
		m_ComboBoxIspDevice.SetCurSel(0);
	}
	return TRUE;
}

void CPY32IspToolDlg::EnableCtrl(BOOL bEnable)
{
	//GetDlgItem(IDC_COMBO_ISP_DEVICE)->EnableWindow(bEnable);
	//GetDlgItem(IDC_BUTTON_DNLOAD_OPEN)->EnableWindow(bEnable);
	GetDlgItem(IDC_RADIO_ERASE_FULL_CHIP)->EnableWindow(bEnable);
	GetDlgItem(IDC_RADIO_ERASE_SECTORS)->EnableWindow(bEnable);
	GetDlgItem(IDC_RADIO_ERASE_PAGES)->EnableWindow(bEnable);
	GetDlgItem(IDC_RADIO_DONOT_ERASE)->EnableWindow(bEnable);
	GetDlgItem(IDC_CHECK_PROGRAM)->EnableWindow(bEnable);
	GetDlgItem(IDC_CHECK_VERIFY)->EnableWindow(bEnable);
	GetDlgItem(IDC_CHECK_PROGRAM_OPTION_BYTES)->EnableWindow(bEnable);
	GetDlgItem(IDC_CHECK_RUN_APP)->EnableWindow(bEnable);
	GetDlgItem(IDC_BUTTON_RUN_APP)->EnableWindow(bEnable);
	GetDlgItem(IDC_BUTTON_UPLOAD)->EnableWindow(bEnable);
	GetDlgItem(IDC_BUTTON_DNLOAD)->EnableWindow(bEnable);
}


void CPY32IspToolDlg::OnBnClickedCheckProgram()
{
	// TODO: 在此添加控件通知处理程序代码
	FILE_InitStruct.PROGRAM_FLASH = ((CButton*)GetDlgItem(IDC_CHECK_PROGRAM))->GetCheck() ? 1 : 0;
}


void CPY32IspToolDlg::OnBnClickedCheckVerify()
{
	// TODO: 在此添加控件通知处理程序代码
	FILE_InitStruct.VERIFY_FLASH = ((CButton*)GetDlgItem(IDC_CHECK_VERIFY))->GetCheck() ? 1 : 0;
}


void CPY32IspToolDlg::OnBnClickedCheckProgramOptionBytes()
{
	// TODO: 在此添加控件通知处理程序代码
	FILE_InitStruct.PROGRAM_OPTION = ((CButton*)GetDlgItem(IDC_CHECK_PROGRAM_OPTION_BYTES))->GetCheck() ? 1 : 0;
	if (FILE_InitStruct.PROGRAM_OPTION) {
		COptionBytesDlg dlg;
		dlg.m_pucOptionBytes = FILE_InitStruct.OPTION_BYTES;
		dlg.m_wPID = m_wPID;
		dlg.DoModal();
	}
}


void CPY32IspToolDlg::OnBnClickedCheckRunApp()
{
	// TODO: 在此添加控件通知处理程序代码
	FILE_InitStruct.DONE_OPERATE = ((CButton*)GetDlgItem(IDC_CHECK_RUN_APP))->GetCheck() ? 1 : 0;
}

void CPY32IspToolDlg::OnBnClickedButtonDnloadOpen()
{
	// TODO: 在此添加控件通知处理程序代码
	CFileDialog FileDialog(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("Hex/Bin Files(*.hex;*.bin)|*.hex;*.bin||"), NULL);

	if (IDOK == FileDialog.DoModal())
	{
		OpenFile(FileDialog.GetPathName());
	}
}

BOOL CPY32IspToolDlg::OpenFile(LPCTSTR lpszPathName)
{
	DWORD dwAddr;

	memset(m_pucProgramMemory, 0xFF, MAX_MEMORY_SIZE);
	FILE_InitStruct.BASE_ADDR = 0x00000000;
	FILE_InitStruct.CODE_SIZE = 0x00000000;

	CMemoryFile MemoryFile;
	if (!MemoryFile.OpenMemoryFile(lpszPathName, m_pucProgramMemory, FILE_InitStruct.BASE_ADDR, FILE_InitStruct.CODE_SIZE))
	{
		UpdateStatus(_T("Open %s fail."), lpszPathName);
		return FALSE;
	}

	if (0x00000000 == FILE_InitStruct.BASE_ADDR) {
		CSetAddrSizeDlg dlg;

		FILE_InitStruct.BASE_ADDR = 0x08000000;

		dlg.m_dwAddr = FILE_InitStruct.BASE_ADDR;
		dlg.m_dwSize = FILE_InitStruct.CODE_SIZE;

		if (IDOK == dlg.DoModal())
		{
			dwAddr = dlg.m_dwAddr;
			FILE_InitStruct.BASE_ADDR = dwAddr & 0xFF000000;
			FILE_InitStruct.DATA_ADDR = dwAddr & 0x00FFFF00;
			FILE_InitStruct.CODE_SIZE = dlg.m_dwSize;
			memcpy(m_pucTargetMemory, m_pucProgramMemory, FILE_InitStruct.CODE_SIZE);
			memset(m_pucProgramMemory, 0xFF, FILE_InitStruct.DATA_ADDR);
			memcpy(m_pucProgramMemory + FILE_InitStruct.DATA_ADDR, m_pucTargetMemory, FILE_InitStruct.CODE_SIZE);
		}
	}

	FILE_InitStruct.CODE_SIZE = (FILE_InitStruct.CODE_SIZE + CHIP_InitStruct.SECTOR_SIZE - 1) & ~(CHIP_InitStruct.SECTOR_SIZE - 1);

	dwAddr = 0x00000000;

	while (dwAddr < CHIP_InitStruct.FLASH_SIZE)
	{
		if (0xFFFFFFFF != M32(m_pucProgramMemory + dwAddr))
		{
			FILE_InitStruct.DATA_ADDR = dwAddr;
			break;
		}
		dwAddr += 4;
	}

	FILE_InitStruct.FLASH_CHECKSUM = GetCheckSum(m_pucProgramMemory + FILE_InitStruct.DATA_ADDR, FILE_InitStruct.CODE_SIZE);
	FILE_InitStruct.FLASH_CRC = GetCrc16(m_pucProgramMemory + FILE_InitStruct.DATA_ADDR, FILE_InitStruct.CODE_SIZE);

	GetDlgItem(IDC_EDIT_DNLOAD_FILE)->SetWindowText(lpszPathName);

	UpdateStatus(_T("Application addresss 0x%08X, checksum 0x%08X."), FILE_InitStruct.BASE_ADDR + FILE_InitStruct.DATA_ADDR, FILE_InitStruct.FLASH_CHECKSUM);

	AfxGetApp()->WriteProfileString(_T("Settings"), _T("FileName"), lpszPathName);

	return TRUE;
}

void CPY32IspToolDlg::UpdateStatus(const TCHAR* szFormat, ...)
{
	TCHAR szContent[1024] = { 0 };

	va_list arglist;
	va_start(arglist, szFormat);
	_vsntprintf_s(szContent, 1024, szFormat, arglist);
	va_end(arglist);

	TRACE(_T("%s\n"), szContent);

	m_EditLog.SetSel(m_EditLog.GetWindowTextLength(), -1);

	m_EditLog.ReplaceSel(szContent);
	m_EditLog.ReplaceSel(_T("\r\n"));
}

UINT CPY32IspToolDlg::ThreadProc(LPVOID lParam)
{
	CPY32IspToolDlg* pThis = (CPY32IspToolDlg*)lParam;
	pThis->ThreadFunc();
	return 0;
}

void CPY32IspToolDlg::ThreadFunc()
{
	uint32_t nErrCode = ERROR_SUCCESS;

	EnableCtrl(FALSE);

	FILE_InitStruct.PRO_MODE = PRO_ISP;

	m_EditLog.SetWindowText(_T(""));
	m_XProgressCtrl.SetBarBkColor(RGB(235, 235, 235));
	m_XProgressCtrl.SetWindowText(_T(""));
	m_XProgressCtrl.Invalidate();
	UpdateStatus(_T("Start %s at %s"), FUNC_TEXT[m_eThreadFuncID], CTime::GetCurrentTime().Format(_T("%H:%M:%S")));

	nErrCode = TargetConnect();
	if (ERROR_SUCCESS != nErrCode) {
		goto Exit_ThreadFunc;
	}

	switch (m_eThreadFuncID)
	{
	case ID_TargetConnect:
		nErrCode = TargetConnect();
		break;
	case ID_TargetDisconnect:
		nErrCode = TargetDisconnect();
		break;
	case ID_TargetDownload:
		nErrCode = TargetDownload();
		break;
	case ID_TargetEraseFullChip:
		nErrCode = TargetEraseFullChip();
		break;
	case ID_TargetEraseSectors:
		nErrCode = TargetEraseSectors();
		break;
	case ID_TargetBlankCheck:
		nErrCode = TargetBlankCheck();
		break;
	case ID_TargetProgram:
		nErrCode = TargetProgram();
		break;
	case ID_TargetVerify:
		nErrCode = TargetVerify();
		break;
	case ID_TargetReadData:
		nErrCode = TargetReadData();
		break;
	case ID_TargetRunApp:
		nErrCode = TargetRunApp();
		//TargetDisconnect();
		break;
	case ID_TargetProgramOptionBytes:
		nErrCode = TargetProgramOptionBytes();
		break;
	}

Exit_ThreadFunc:
	TargetDisconnect();

	switch (nErrCode)
	{
	case ERROR_SUCCESS:
		m_XProgressCtrl.SetBarBkColor(RGB(0, 155, 0));
		m_XProgressCtrl.SetWindowText(_T("PASS"));
		break;
	default:
		m_XProgressCtrl.SetBarBkColor(RGB(255, 0, 0));
		m_XProgressCtrl.SetWindowText(error_message[nErrCode]);
		UpdateStatus(error_message[nErrCode]);
		UpdateStatus(_T("Error: %s failed."), FUNC_TEXT[m_eThreadFuncID]);		
		break;
	}	

	m_XProgressCtrl.SetPos(0);	
	m_XProgressCtrl.Invalidate();

	EnableCtrl(TRUE);

	UpdateStatus(_T("%s finished at %s"), FUNC_TEXT[m_eThreadFuncID], CTime::GetCurrentTime().Format(_T("%H:%M:%S")));

	if ((ERROR_SUCCESS == nErrCode) && m_bExitApp)
	{
		PostMessage(WM_CLOSE, 0, 0);
		return;
	}
}

uint8_t  CPY32IspToolDlg::ReadMemory(uint32_t addr, uint8_t* data, uint32_t size)
{
	if (m_bReadMemoryOneTime)
	{
		return m_pDevice->ReadMemory(addr, data, size);
	}
	else
	{		
		uint32_t i = 0;
		uint32_t nErrorCode = ERROR_SUCCESS;

		for (i=0; i<size/0x20; i++)
		{
			nErrorCode = m_pDevice->ReadMemory(addr + i * 0x20, data + i * 0x20, 0x20);
			if (ERROR_SUCCESS != nErrorCode)
			{
				return nErrorCode;
			}
		}

		if (size % 0x20)
		{
			return m_pDevice->ReadMemory(addr + i * 0x20, data + i * 0x20, size % 0x20);
		}
	}

	return ERROR_SUCCESS;
}

uint32_t CPY32IspToolDlg::TargetDownload()
{
	uint32_t nErrCode = ERROR_SUCCESS;

	switch (FILE_InitStruct.ERASE_MODE)
	{
	case 1:
		nErrCode = TargetEraseFullChip();
		break;
	case 2:
		nErrCode = TargetEraseSectors();
		break;
	case 3:
		nErrCode = TargetErasePages();
		break;
	}
	if (ERROR_SUCCESS != nErrCode) {
		return nErrCode;
	}

	if (FILE_InitStruct.PROGRAM_FLASH) {
		nErrCode = TargetProgram();
		if (ERROR_SUCCESS != nErrCode) {
			return nErrCode;
		}
	}

	if (FILE_InitStruct.VERIFY_FLASH) {
		nErrCode = TargetVerify();
		if (ERROR_SUCCESS != nErrCode) {
			return nErrCode;
		}
	}

	if (FILE_InitStruct.PROGRAM_OPTION) {
		nErrCode = TargetProgramOptionBytes();
		if (ERROR_SUCCESS != nErrCode) {
			return nErrCode;
		}
	}

	if (FILE_InitStruct.DONE_OPERATE) {
		nErrCode = TargetRunApp();
		if (ERROR_SUCCESS != nErrCode) {
			return nErrCode;
		}
	}

	return nErrCode;
}

uint32_t CPY32IspToolDlg::TargetConnect()
{
	uint32_t nErrCode = ERROR_SUCCESS;

	//UpdateStatus(DTR_RTS_STR[FILE_InitStruct.DTR_RTS]);

	m_pDevice = new CPuyaISP;

	m_pDevice->m_ucDtrRts = FILE_InitStruct.DTR_RTS;

	nErrCode = m_pDevice->Init(m_dwComPort, FILE_InitStruct.BAUD_RATE);

	if (ERROR_SUCCESS != nErrCode) {
		return nErrCode;
	}

	AfxGetApp()->WriteProfileInt(_T("Settings"), _T("ComPort"), m_dwComPort);

	SetStructData(m_pDevice->m_product_id);

	nErrCode = TargetReadMcuInfo();

	return nErrCode;
}

uint32_t CPY32IspToolDlg::TargetReadMcuInfo()
{
	BYTE ucData[0x400];
	CString csText(_T(""));
	DWORD dwFlashSize, dwSramSize;
	BYTE optr;

	if (ERROR_SUCCESS != ReadMemory(CHIP_InitStruct.OPTION_ADDR, ucData, CHIP_InitStruct.OPTION_SIZE)) {
		return ERROR_IAP_READ;
	}
	optr = ucData[0x01];
	UpdateStatus(_T("Getting option bytes successfully!"));
	csText.Empty();
	for (DWORD i = 0; i < CHIP_InitStruct.OPTION_SIZE; i++) {
		if (i && (0 == i % 0x10)) {
			csText.AppendFormat(_T("\r\n"));
		}
		csText.AppendFormat(_T("%02X "), ucData[i]);
	}
	UpdateStatus(csText);

	if (ERROR_SUCCESS != ReadMemory(CHIP_InitStruct.FLASHSIZE_ADDR, ucData, 0x01))
	{
		return ERROR_GET_FLASH_SRAM_SIZE;
	}

	dwFlashSize = CHIP_InitStruct.FLASH_SIZE;
	dwSramSize = CHIP_InitStruct.RAM_SIZE;

	switch (m_wPID)
	{
	case 0x0068:
		dwFlashSize = 32 * 0x400;// (((ucData[0] & 0x03) >> 0) + 1) * 8 * 0x400;
		dwSramSize = 4 * 0x400;// (((ucData[0] & 0x10) >> 4) + 1) * 2 * 0x400;
		break;
	case 0x0064:
		ucData[0] = min(ucData[0], 4);
		dwFlashSize = (24 - ucData[0]) * 0x400;
		break;
	case 0x0440:
		dwFlashSize = (((ucData[0] & 0x07) >> 0) + 1) * 8 * 0x400;
		dwSramSize = (((ucData[0] & 0x30) >> 4) + 1) * 2 * 0x400;
		m_bReadMemoryOneTime = READ_BIT(optr, 1 << (13 - 8)) ? TRUE : FALSE;
		break;
	case 0x0444:
		dwFlashSize = (((ucData[0] & 0x03) >> 0) + 1) * 16 * 0x400;
		dwSramSize = (((ucData[0] & 0x30) >> 4) + 1) * 2 * 0x400;
		m_bReadMemoryOneTime = READ_BIT(optr, 1 << (13 - 8)) ? TRUE : FALSE;
		break;
	case 0x0063:
	case 0x0448:
		dwFlashSize = (((ucData[0] & 0x03) >> 0) + 1) * 32 * 0x400;
		dwSramSize = (((ucData[0] & 0x0C) >> 2) + 1) * 4 * 0x400;
		break;
	case 0x0100:
	case 0x0413:
		dwFlashSize = (((ucData[0] & 0x1F) >> 0) + 1) * 16 * 0x400;
		dwSramSize = (((ucData[0] & 0xE0) >> 5) + 1) * 8 * 0x400;
		break;
	}

	CHIP_InitStruct.FLASH_SIZE = min(dwFlashSize, CHIP_InitStruct.FLASH_SIZE);
	CHIP_InitStruct.RAM_SIZE = min(dwSramSize, CHIP_InitStruct.RAM_SIZE);
	UpdateStatus(_T("Getting flash and sram size successfully!\r\nFlash size %dKB, Sram size %dKB."), CHIP_InitStruct.FLASH_SIZE / 0x400, CHIP_InitStruct.RAM_SIZE / 0x400);

	if (ERROR_SUCCESS != ReadMemory(CHIP_InitStruct.UID_ADDR, ucData, 12/*CHIP_InitStruct.UID_SIZE*/))
	{
		return ERROR_GET_UID;
	}
	UpdateStatus(_T("Getting UID successfully!"));
	csText.Empty();
	for (int i = 0; i < 12/*CHIP_InitStruct.UID_SIZE*/; i++) {
		//if (i && (0 == i % 0x10)) {
		//	csText.AppendFormat(_T("\r\n"));
		//}
		csText.AppendFormat(_T("%02X "), ucData[i]);
	}
	UpdateStatus(csText);

	return ERROR_SUCCESS;
}

uint32_t CPY32IspToolDlg::TargetDisconnect()
{
	uint32_t nErrCode = ERROR_SUCCESS;
	
	nErrCode = m_pDevice->Uninit();

	delete m_pDevice;

	m_pDevice = NULL;

	return nErrCode;
}

uint32_t CPY32IspToolDlg::TargetEraseFullChip()
{
	uint32_t nErrCode = ERROR_SUCCESS;

	m_XProgressCtrl.SetWindowText(_T("Erasing..."));

	nErrCode = m_pDevice->EraseChip();
	if (ERROR_SUCCESS != nErrCode) {
		UpdateStatus(_T("EraseChip Fail."));
	}
	else {
		UpdateStatus(_T("EraseChip Done."));
	}
	m_XProgressCtrl.SetWindowText(_T(""));

	return nErrCode;
}

uint32_t CPY32IspToolDlg::TargetEraseSectors()
{
	uint8_t* data;
	uint32_t nErrCode = ERROR_SUCCESS;
	uint32_t addr, size = 0;

	data = new uint8_t[2 * (FILE_InitStruct.CODE_SIZE / CHIP_InitStruct.SECTOR_SIZE)];

	m_XProgressCtrl.SetWindowText(_T("Erasing..."));

	addr = FILE_InitStruct.DATA_ADDR;
	while ((addr - FILE_InitStruct.DATA_ADDR) < FILE_InitStruct.CODE_SIZE)
	{
		for (uint32_t j = 0; j < CHIP_InitStruct.SECTOR_SIZE; j++)
		{
			if (0xFF != m_pucProgramMemory[addr + j])
			{
				data[2 * size + 0] = ((addr / CHIP_InitStruct.SECTOR_SIZE) >> 8) & 0xFF;
				data[2 * size + 1] = ((addr / CHIP_InitStruct.SECTOR_SIZE) >> 0) & 0xFF;
				size++;
				break;
			}
		}
		addr += CHIP_InitStruct.SECTOR_SIZE;
	}
	for (uint32_t i = 0; i < size / 0x10; i++) {
		addr = i * CHIP_InitStruct.SECTOR_SIZE * 0x10;
		nErrCode = m_pDevice->EraseSector(&data[i * 0x10 * 2], 0x10);
		if (ERROR_SUCCESS != nErrCode) {
			break;
		}
	}
	if (size % 0x10) {
		nErrCode = m_pDevice->EraseSector(&data[( size / 0x10) * 0x10 * 2], size % 0x10);
	}

	if (ERROR_SUCCESS != nErrCode) {
		UpdateStatus(_T("EraseSector Fail at: %08XH."), addr);
	}
	else {
		UpdateStatus(_T("EraseSector Done."));
	}

	m_XProgressCtrl.SetWindowText(_T(""));

	delete[] data;

	return nErrCode;
}

uint32_t CPY32IspToolDlg::TargetErasePages()
{
	uint8_t* data;
	uint32_t nErrCode = ERROR_SUCCESS;
	uint32_t addr, size = 0;

	data = new uint8_t[2 * (FILE_InitStruct.CODE_SIZE / CHIP_InitStruct.PAGE_SIZE)];

	m_XProgressCtrl.SetWindowText(_T("Erasing..."));

	addr = FILE_InitStruct.DATA_ADDR;
	while ((addr - FILE_InitStruct.DATA_ADDR) < FILE_InitStruct.CODE_SIZE)
	{
		for (uint32_t j = 0; j < CHIP_InitStruct.PAGE_SIZE; j++)
		{
			if (0xFF != m_pucProgramMemory[addr + j])
			{
				data[2 * size + 0] = ((addr / CHIP_InitStruct.PAGE_SIZE) >> 8) & 0xFF;
				data[2 * size + 1] = ((addr / CHIP_InitStruct.PAGE_SIZE) >> 0) & 0xFF;
				size++;
				break;
			}
		}
		addr += CHIP_InitStruct.PAGE_SIZE;
	}
	for (uint32_t i = 0; i < size / 0x10; i++) {
		addr = i * CHIP_InitStruct.PAGE_SIZE * 0x10;
		nErrCode = m_pDevice->ErasePage(&data[i * 0x10 * 2], 0x10);
		if (ERROR_SUCCESS != nErrCode) {
			break;
		}
	}
	if (size % 0x10) {
		nErrCode = m_pDevice->ErasePage(&data[(size / 0x10) * 0x10 * 2], size % 0x10);
	}

	if (ERROR_SUCCESS != nErrCode) {
		UpdateStatus(_T("ErasePage Fail at: %08XH."), addr);
	}
	else {
		UpdateStatus(_T("ErasePage Done."));
	}

	m_XProgressCtrl.SetWindowText(_T(""));

	delete[] data;

	return nErrCode;
}

uint32_t CPY32IspToolDlg::TargetBlankCheck()
{
	CString csText;
	uint32_t nErrCode = ERROR_SUCCESS;
	uint32_t dwAddr = FILE_InitStruct.DATA_ADDR;

	m_XProgressCtrl.SetRange(0, (short)(FILE_InitStruct.CODE_SIZE / CHIP_InitStruct.PAGE_SIZE));

	while ((dwAddr - FILE_InitStruct.DATA_ADDR) < FILE_InitStruct.CODE_SIZE)
	{
		m_XProgressCtrl.SetPos(dwAddr / CHIP_InitStruct.PAGE_SIZE);
		csText.Format(_T("BlankCheck: %08XH"), dwAddr);
		m_XProgressCtrl.SetWindowText(csText);

		nErrCode = ReadMemory(FILE_InitStruct.BASE_ADDR + dwAddr, m_pucTargetMemory + dwAddr, CHIP_InitStruct.PAGE_SIZE);
		if (ERROR_SUCCESS != nErrCode) {
			UpdateStatus(_T("Read Failed at: %08XH."), FILE_InitStruct.BASE_ADDR + dwAddr);
			break;
		}
		for (uint32_t j = 0; j < CHIP_InitStruct.PAGE_SIZE; j++) {
			if (0xFF != m_pucTargetMemory[dwAddr + j]) {
				UpdateStatus(_T("Contents mismatch at: %08XH (Flash=%02XH Requitred=%02XH) !"),
					FILE_InitStruct.BASE_ADDR + dwAddr + j, m_pucTargetMemory[dwAddr + j], 0xFF);
				return 2;
			}
		}
		dwAddr += CHIP_InitStruct.PAGE_SIZE;
	}

	UpdateStatus(_T("BlankCheck OK."));

	return nErrCode;
}

uint32_t CPY32IspToolDlg::TargetProgram()
{
	CString csText;
	uint32_t nErrCode = ERROR_SUCCESS;
	uint32_t dwAddr = FILE_InitStruct.DATA_ADDR;

	m_XProgressCtrl.SetRange(0, (short)(FILE_InitStruct.CODE_SIZE / CHIP_InitStruct.PAGE_SIZE));
	
	while ((dwAddr - FILE_InitStruct.DATA_ADDR) < FILE_InitStruct.CODE_SIZE)
	{
		m_XProgressCtrl.SetPos(dwAddr / CHIP_InitStruct.PAGE_SIZE);
		csText.Format(_T("Program: %08XH"), FILE_InitStruct.BASE_ADDR + dwAddr);
		m_XProgressCtrl.SetWindowText(csText);
		for (uint32_t j = 0; j < CHIP_InitStruct.PAGE_SIZE; j++)
		{
			if (0xFF != m_pucProgramMemory[dwAddr + j])
			{
				nErrCode = m_pDevice->WriteMemory(FILE_InitStruct.BASE_ADDR + dwAddr,
					m_pucProgramMemory + dwAddr, CHIP_InitStruct.PAGE_SIZE);
				if (ERROR_SUCCESS != nErrCode) {
					UpdateStatus(_T("Programming Failed at: %08XH."), FILE_InitStruct.BASE_ADDR + dwAddr);
					return nErrCode;
				}

				break;
			}
		}
		dwAddr += CHIP_InitStruct.PAGE_SIZE;
	}

	m_XProgressCtrl.SetPos(FILE_InitStruct.CODE_SIZE / CHIP_InitStruct.PAGE_SIZE);

	UpdateStatus(_T("Programming Done."));

	return nErrCode;
}

uint32_t CPY32IspToolDlg::TargetVerify()
{
	CString csText;
	uint32_t nErrCode = ERROR_SUCCESS;
	uint32_t dwAddr = FILE_InitStruct.DATA_ADDR;

	m_XProgressCtrl.SetRange(0, (short)(FILE_InitStruct.CODE_SIZE / CHIP_InitStruct.PAGE_SIZE));

	while ((dwAddr - FILE_InitStruct.DATA_ADDR) < FILE_InitStruct.CODE_SIZE)
	{
		m_XProgressCtrl.SetPos(dwAddr / CHIP_InitStruct.PAGE_SIZE);
		csText.Format(_T("Verify: %08XH"), FILE_InitStruct.BASE_ADDR + dwAddr);
		m_XProgressCtrl.SetWindowText(csText);
		for (uint32_t j = 0; j < CHIP_InitStruct.PAGE_SIZE; j++)
		{
			if (0xFF != m_pucProgramMemory[dwAddr + j])
			{
				nErrCode = ReadMemory(FILE_InitStruct.BASE_ADDR + dwAddr,
					m_pucTargetMemory + dwAddr, CHIP_InitStruct.PAGE_SIZE);
				if (ERROR_SUCCESS != nErrCode) {
					UpdateStatus(_T("Read Failed at: %08XH."), FILE_InitStruct.BASE_ADDR + dwAddr);
					return nErrCode;
				}
				for (uint32_t k = 0; k < CHIP_InitStruct.PAGE_SIZE; k++) {
					if (m_pucTargetMemory[dwAddr + k] != m_pucProgramMemory[dwAddr + k]) {
						UpdateStatus(_T("Contents mismatch at: %08XH (Flash=%02XH Requitred=%02XH) !"),
							FILE_InitStruct.BASE_ADDR + dwAddr + k, m_pucTargetMemory[dwAddr + k], m_pucProgramMemory[dwAddr + k]);
						return 2;
					}
				}

				break;
			}
		}
		dwAddr += CHIP_InitStruct.PAGE_SIZE;
	}
	m_XProgressCtrl.SetPos(FILE_InitStruct.CODE_SIZE / CHIP_InitStruct.PAGE_SIZE);

	UpdateStatus(_T("Verify OK."));

	return nErrCode;
}

uint32_t CPY32IspToolDlg::TargetReadData()
{
	CString csText;
	uint32_t nErrCode = ERROR_SUCCESS;
	uint32_t dwAddr = 0;
	CSetAddrSizeDlg dlg;

	dlg.m_dwAddr = CHIP_InitStruct.FLASH_ADDR;
	dlg.m_dwSize = CHIP_InitStruct.FLASH_SIZE;

	dlg.DoModal();

	m_XProgressCtrl.SetRange(0, (short)(dlg.m_dwSize / CHIP_InitStruct.PAGE_SIZE));
	while ((dwAddr / CHIP_InitStruct.PAGE_SIZE) < (dlg.m_dwSize / CHIP_InitStruct.PAGE_SIZE))
	{
		m_XProgressCtrl.SetPos(dwAddr / CHIP_InitStruct.PAGE_SIZE);
		csText.Format(_T("Read: %08XH"), dlg.m_dwAddr + dwAddr);
		m_XProgressCtrl.SetWindowText(csText);

		nErrCode = ReadMemory(dlg.m_dwAddr + dwAddr, m_pucTargetMemory + dwAddr, CHIP_InitStruct.PAGE_SIZE);
		if (ERROR_SUCCESS != nErrCode) {
			UpdateStatus(_T("Read Failed at: %08XH."), dlg.m_dwAddr + dwAddr);
			return nErrCode;
		}

		dwAddr += CHIP_InitStruct.PAGE_SIZE;
	}

	if (dlg.m_dwSize % CHIP_InitStruct.PAGE_SIZE)
	{
		nErrCode = ReadMemory(dlg.m_dwAddr + dwAddr, m_pucTargetMemory + dwAddr, dlg.m_dwSize % CHIP_InitStruct.PAGE_SIZE);
		if (ERROR_SUCCESS != nErrCode) {
			UpdateStatus(_T("Read Failed at: %08XH."), dlg.m_dwAddr + dwAddr);
			return nErrCode;
		}
	}

	UpdateStatus(_T("Read Done."));

	CMemoryFile MemoryFile;
	MemoryFile.SaveMemoryFileEx(m_pucTargetMemory, dlg.m_dwAddr, dlg.m_dwSize);

	return nErrCode;
}

uint32_t CPY32IspToolDlg::TargetRunApp()
{
	uint32_t nErrCode = ERROR_SUCCESS;

	nErrCode = m_pDevice->Go(FILE_InitStruct.BASE_ADDR + FILE_InitStruct.DATA_ADDR);
	if (ERROR_SUCCESS != nErrCode) {
		UpdateStatus(_T("Run App Fail."));
	}
	else {
		UpdateStatus(_T("Application running ..."));
	}

	return nErrCode;
}

uint32_t CPY32IspToolDlg::TargetProgramOptionBytes()
{
	CString csText;
	uint8_t ucData[0x80];
	uint32_t nErrCode = ERROR_SUCCESS;

	if (ERROR_SUCCESS != m_pDevice->WriteMemory(CHIP_InitStruct.OPTION_ADDR, FILE_InitStruct.OPTION_BYTES, CHIP_InitStruct.OPTION_SIZE)) {
		UpdateStatus(_T("Program option bytes Fail."));
	}
	else {
		UpdateStatus(_T("Program option bytes Done."));
	}

	if (RDP_LEVEL_0 != FILE_InitStruct.OPTION_BYTES[0x00])
	{
		return ERROR_SUCCESS;
	}

	m_pDevice->Uninit();

	Sleep(500);

	nErrCode = m_pDevice->Init(m_dwComPort, FILE_InitStruct.BAUD_RATE);
	if (ERROR_SUCCESS != nErrCode) {
		return nErrCode;
	}

	if (ERROR_SUCCESS != ReadMemory(CHIP_InitStruct.OPTION_ADDR, ucData, CHIP_InitStruct.OPTION_SIZE)) {
		//UpdateStatus(_T("Read option bytes Error."));
		return ERROR_IAP_READ;
	}

	UpdateStatus(_T("Getting option bytes successfully!"));
	csText.Empty();
	for (DWORD i = 0; i < CHIP_InitStruct.OPTION_SIZE; i++) {
		if (i && (0 == i % 0x10)) {
			csText.AppendFormat(_T("\r\n"));
		}
		csText.AppendFormat(_T("%02X "), ucData[i]);
	}
	UpdateStatus(csText);

	switch (m_wPID)
	{
	case 0x0440:
		if ((M32(&FILE_InitStruct.OPTION_BYTES[0x00]) != M32(&ucData[0x00]))
		 || (M32(&FILE_InitStruct.OPTION_BYTES[0x04]) != M32(&ucData[0x04]))
		 || (M32(&FILE_InitStruct.OPTION_BYTES[0x0C]) != M32(&ucData[0x0C])))
		{
			nErrCode = ERROR_VERIFY_OPTION_BYTES;
		}
		break;
	case 0x0064:
	case 0x0068:
	case 0x0444:
		if ((M32(&FILE_InitStruct.OPTION_BYTES[0x00]) != M32(&ucData[0x00]))
		 || (M32(&FILE_InitStruct.OPTION_BYTES[0x04]) != M32(&ucData[0x04]))
	     || (M32(&FILE_InitStruct.OPTION_BYTES[0x08]) != M32(&ucData[0x08]))
		 || (M32(&FILE_InitStruct.OPTION_BYTES[0x0C]) != M32(&ucData[0x0C])))
		{
			nErrCode = ERROR_VERIFY_OPTION_BYTES;
		}
		break;
	case 0x0063:
	case 0x0448:
		if ( (M32(&FILE_InitStruct.OPTION_BYTES[0x00]) != M32(&ucData[0x00]))
		  || (M32(&FILE_InitStruct.OPTION_BYTES[0x08]) != M32(&ucData[0x08]))
		  || (M32(&FILE_InitStruct.OPTION_BYTES[0x18]) != M32(&ucData[0x18])) )
		{
			nErrCode = ERROR_VERIFY_OPTION_BYTES;
		}
		break;
	case 0x0100:
	case 0x0413:
		if ( (M32(&FILE_InitStruct.OPTION_BYTES[0x00]) != M32(&ucData[0x00]))
		  || (M32(&FILE_InitStruct.OPTION_BYTES[0x0C]) != M32(&ucData[0x0C])) )
		{
			nErrCode = ERROR_VERIFY_OPTION_BYTES;
		}
		break;
	}

	//UpdateStatus(_T("Verify option bytes Error."));
	if (ERROR_SUCCESS == nErrCode)
		UpdateStatus(_T("Verify option bytes successfully."));

	return nErrCode;
}

void CPY32IspToolDlg::OnBnClickedButtonRunApp()
{
	// TODO: 在此添加控件通知处理程序代码
	m_eThreadFuncID = ID_TargetRunApp;
	AfxBeginThread(ThreadProc, this);
}

void CPY32IspToolDlg::OnBnClickedButtonUpload()
{
	// TODO: 在此添加控件通知处理程序代码
	m_eThreadFuncID = ID_TargetReadData;
	AfxBeginThread(ThreadProc, this);
}

void CPY32IspToolDlg::OnBnClickedButtonDnload()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();

	FILE_InitStruct.ERASE_MODE = m_nRadioErase;

	m_eThreadFuncID = ID_TargetDownload;
	AfxBeginThread(ThreadProc, this);
}


void CPY32IspToolDlg::OnCbnSelchangeComboIspDevice()
{
	// TODO: 在此添加控件通知处理程序代码
}


void CPY32IspToolDlg::OnBnClickedButtonConnect()
{
	// TODO: 在此添加控件通知处理程序代码
	CString csWindowText;
	uint32_t nErrCode;

	m_ComboBoxIspDevice.GetWindowText(csWindowText);
	csWindowText = csWindowText.Right(csWindowText.GetLength() - csWindowText.Find(_T("(COM")) - 4);
	csWindowText.Replace(_T(")"), _T(""));
	m_dwComPort = _tcstol(csWindowText, NULL, 10);

	nErrCode = TargetConnect();

	EnableCtrl(ERROR_SUCCESS == nErrCode);

	switch (nErrCode)
	{
	case ERROR_SUCCESS:
		m_XProgressCtrl.SetBarBkColor(RGB(0, 155, 0));
		m_XProgressCtrl.SetWindowText(_T("Connect PASS"));
		break;
	default:
		m_XProgressCtrl.SetBarBkColor(RGB(255, 0, 0));
		m_XProgressCtrl.SetWindowText(_T("Connect FAIL"));
		break;
	}

	m_XProgressCtrl.SetPos(0);
	m_XProgressCtrl.Invalidate();

	TargetDisconnect();
}


void CPY32IspToolDlg::OnCbnSelchangeComboBaudrate()
{
	// TODO: 在此添加控件通知处理程序代码
	CString csWindowText;

	m_ComboBoxBaudrate.GetWindowText(csWindowText);

	FILE_InitStruct.BAUD_RATE = _tcstol(csWindowText, NULL, 10);

	AfxGetApp()->WriteProfileInt(_T("Settings"), _T("BaudRate"), FILE_InitStruct.BAUD_RATE);
}


void CPY32IspToolDlg::OnCbnSelchangeComboDtrRts()
{
	// TODO: 在此添加控件通知处理程序代码
	FILE_InitStruct.DTR_RTS = m_HcComboBoxDtrRts.GetCurSel();

	AfxGetApp()->WriteProfileInt(_T("Settings"), _T("DtrRts"), FILE_InitStruct.DTR_RTS);
}


BOOL CPY32IspToolDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if (WM_KEYDOWN == pMsg->message)
	{
		if (VK_SPACE == pMsg->wParam || VK_RETURN == pMsg->wParam)
		{
			OnBnClickedButtonDnload();
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}
