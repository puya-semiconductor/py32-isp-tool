//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 PY32IspTool.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PY32ISPTOOL_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP_PUYA                 129
#define IDD_DIALOG_OPTION_BYTES         135
#define IDD_DIALOG_ADDR_SIZE            140
#define IDC_COMBO1                      1000
#define IDC_COMBO_ISP_DEVICE            1000
#define IDC_BUTTON_LEAVE_ISP            1001
#define IDC_BUTTON_RUN_APP              1001
#define IDC_RADIO_DNLOAD                1002
#define IDC_BUTTON_DNLOAD               1002
#define IDC_RADIO_UPLOAD                1003
#define IDC_BUTTON_UPLOAD               1003
#define IDC_EDIT_DNLOAD_FILE            1004
#define IDC_BUTTON_DNLOAD_OPEN          1005
#define IDC_EDIT_UPLOAD_FILE            1006
#define IDC_BUTTON_CONNECT              1006
#define IDC_BUTTON_UPLOAD_OPEN          1007
#define IDC_COMBO_BAUDRATE              1007
#define IDC_COMBO2                      1008
#define IDC_COMBO_GO_ADDR               1008
#define IDC_COMBO_APP_ADDR              1008
#define IDC_CHECK_VERIFY                1009
#define IDC_CHECK_ERASE_ALL             1010
#define IDC_CHECK_PROGRAM               1010
#define IDC_PROGRESS                    1011
#define IDC_LIST                        1012
#define IDC_CHECK_RUN_APP               1012
#define IDC_EDIT1                       1013
#define IDC_EDIT_LOG                    1013
#define IDC_EDIT_ADDR                   1013
#define IDC_CHECK_OPTION_BYTES          1014
#define IDC_CHECK_PROGRAM_OPTION_BYTES  1014
#define IDC_EDIT_SIZE                   1014
#define IDC_RADIO_DONOT_ERASE           1015
#define IDC_RADIO_ERASE_FULL_CHIP       1016
#define IDC_STATIC_FLASH_OPTR           1016
#define IDC_RADIO_ERASE_SECTORS         1017
#define IDC_COMBO_DTR_RTS               1018
#define IDC_RADIO_ERASE_PAGES           1019

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
