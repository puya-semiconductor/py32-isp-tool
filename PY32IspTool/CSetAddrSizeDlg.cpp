﻿// CSetAddrSizeDlg.cpp: 实现文件
//

#include "pch.h"
#include "PY32IspTool.h"
#include "afxdialogex.h"
#include "CSetAddrSizeDlg.h"


// CSetAddrSizeDlg 对话框

IMPLEMENT_DYNAMIC(CSetAddrSizeDlg, CDialogEx)

CSetAddrSizeDlg::CSetAddrSizeDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG_ADDR_SIZE, pParent)
	, m_dwAddr(0)
	, m_dwSize(0)
{

}

CSetAddrSizeDlg::~CSetAddrSizeDlg()
{
}

void CSetAddrSizeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSetAddrSizeDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CSetAddrSizeDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CSetAddrSizeDlg 消息处理程序


BOOL CSetAddrSizeDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	CString csWndText;

	csWndText.Format(_T("%08X"), m_dwAddr);
	GetDlgItem(IDC_EDIT_ADDR)->SetWindowText(csWndText);

	csWndText.Format(_T("%08X"), m_dwSize);
	GetDlgItem(IDC_EDIT_SIZE)->SetWindowText(csWndText);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CSetAddrSizeDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CString csWndText;

	GetDlgItem(IDC_EDIT_ADDR)->GetWindowText(csWndText);
	m_dwAddr = _tcstol(csWndText, NULL, 16);

	GetDlgItem(IDC_EDIT_SIZE)->GetWindowText(csWndText);
	m_dwSize = _tcstol(csWndText, NULL, 16);

	CDialogEx::OnOK();
}
